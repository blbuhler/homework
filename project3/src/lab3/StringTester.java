package lab3;

import java.util.Formatter;

public class StringTester {
	
	public static void main(String[] args) {
		// Convert an int to String
		//int x = 42;
		//String s = "" + x;
		//String s2 = String.format(%01.2f, x);
		// Convert a String to int
		//String s1 = "42";
		//One way is to use the parseInt method in the class Integer
		//int x2 = Integer.parseInt(s1);
		// if you try to call parseInt on a String that really doesn't represent an integer, like "Hello" or "23Skidoo".
		//System.out.println(s);
		//System.out.println(s2);
		//System.out.println(x2);
		//System.out.println(Integer.parseInt("Hello"));
		int x1 = 0;
		int x2 = x1;
		x1 ++;
		System.out.println(x2);
		System.out.println(x1);
	}
	
}
