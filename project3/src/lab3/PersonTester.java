package lab3;

public class PersonTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person p = new Person("abc", 123);
		// Get the result of  calling the getName and getAge methods.
		System.out.println(p.getName());
		System.out.println(p.getAge());
		//  For numbers, the default value is always zero. But for objects, the default value is the special value null.
		// Get the result of  calling the getNameLength 
		System.out.println(p.getNameLength());
	}

}
