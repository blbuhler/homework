package lab3;
import java.util.Random;

public class RabbitModel3 {
	// The increase in population each year is a random value in the range 0 through 10. 
	private int population;
	public RabbitModel3()
	{
		reset();
	}
	
	public int getPopulation () 
	{
		return population;
	}
	
	public void simulateYear()
	{
		Random rand = new Random();
		population =+ rand.nextInt(11);
	}
	
	public void reset()
	{
		population = 2 ;
	}
	
	//Refer back to this previous page, item 5, for an example of how to use the class java.util.
	//Random to generate random numbers in a specified range.
	// it is important to create just one instance of Random and use it as the sole source of randomness. You should define an instance variable of type Random and initialize it in the constructor (and/or in the reset() method).
	// Use a "seed" value so that the results of the simulation can be reproduced.
}
