package lab3;

public class RabbitModel4 {
	private int lastYearPopulation;
	private int population;
	public RabbitModel4()
	{
		reset();
	}
	
	public int getPopulation () 
	{
		return population;
	}
	
	public void simulateYear()
	{
		int tempPopulation = population;
		population =+ lastYearPopulation; 
		lastYearPopulation = tempPopulation;
	}
	
	public void reset()
	{
		lastYearPopulation = 1;
		population = 1 ;
	}
}
