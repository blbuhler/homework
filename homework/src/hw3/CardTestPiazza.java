package hw3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import api.Card;
import api.Hand;
import api.IEvaluator;
import api.Suit;

public class CardTestPiazza {
	Card[] AsAdKh7c5d = { new Card(1, Suit.SPADES), new Card(1, Suit.DIAMONDS),
			new Card(13, Suit.HEARTS), new Card(7, Suit.CLUBS),
			new Card(5, Suit.DIAMONDS) };
	Card[] AsAdKh5c5d = { new Card(1, Suit.SPADES), new Card(1, Suit.DIAMONDS),
			new Card(13, Suit.HEARTS), new Card(5, Suit.CLUBS),
			new Card(5, Suit.DIAMONDS) };
	Card[] Ah10c9s8d7c = { new Card(1, Suit.HEARTS), new Card(10, Suit.CLUBS),
			new Card(9, Suit.SPADES), new Card(8, Suit.DIAMONDS),
			new Card(7, Suit.CLUBS) };
	Card[] Ah9s9h9d7c = { new Card(1, Suit.HEARTS), new Card(9, Suit.SPADES),
			new Card(9, Suit.HEARTS), new Card(9, Suit.DIAMONDS),
			new Card(7, Suit.CLUBS) };
	Card[] Ah9s9h9d9c = { new Card(1, Suit.HEARTS), new Card(9, Suit.SPADES),
			new Card(9, Suit.HEARTS), new Card(9, Suit.DIAMONDS),
			new Card(9, Suit.CLUBS) };
	Card[] tend10c10h5s5c = { new Card(10, Suit.HEARTS),
			new Card(10, Suit.DIAMONDS), new Card(10, Suit.CLUBS),
			new Card(5, Suit.SPADES), new Card(5, Suit.CLUBS) };
	Card[] tend10c5s5h5c = { new Card(10, Suit.CLUBS),
			new Card(10, Suit.DIAMONDS), new Card(5, Suit.SPADES),
			new Card(5, Suit.HEARTS), new Card(5, Suit.CLUBS) };
	Card[] JsJh6h4d4c = Card.createArray("Js, Jh, 6h, 4d, 4c");
	Card[] JsJhJc6h4s4d4c = Card.createArray("Js, Jh, Jc, 6h, 4s, 4d, 4c");
	Card[] JsJhJd3s3c = Card.createArray("Js, Jh, Jd, 3s, 3c");
	Card[] AhKdQsJc10d = Card.createArray("Ah, Kd, Qs, Jc, 10d");
	Card[] KhQhJh10h9h = Card.createArray("Kh, Qh, Jh, 10h, 9h");

	@Test
	public void straightSimple() {
		IEvaluator s = new StraightEvaluator(4, 5, 13);
		String msg = "The cards Ah, Kd, Qs, Jc, and 10d should satisfy a straight evaluator: ";
		assertTrue(msg, s.canSatisfy(AhKdQsJc10d));
	}

	@Test
	public void straightMissingAQ() {
		IEvaluator s = new StraightEvaluator(4, 4, 13);
		Card[] notStraight = { AhKdQsJc10d[0], AhKdQsJc10d[1], AhKdQsJc10d[3],
				AhKdQsJc10d[4] };
		String msg = "The cards Ah, Kd, Jc, and 10d should not satisfy.";
		assertFalse(msg, s.canSatisfy(notStraight));
	}

	@Test
	public void straightMissingAMaxCard() {
		IEvaluator s = new StraightEvaluator(4, 4, 13);
		Card[] notStraight = { AhKdQsJc10d[0], AhKdQsJc10d[2], AhKdQsJc10d[3],
				AhKdQsJc10d[4] };
		String msg = "The cards Ah, Qs, Jc, and 10d should not satisfy.";
		assertFalse(msg, s.canSatisfy(notStraight));
	}

	@Test
	public void straightCreateHand() {
		IEvaluator s = new StraightEvaluator(4, 5, 13);
		int[] subset = { 0, 1, 2, 3, 4 };
		Hand created = s.createHand(AhKdQsJc10d, subset);
		String msg = "The cards Ah, Kd, Qs, Jc, and 10d create a straight hand: ";
		assertEquals(msg, "Straight (4) [Ah Kd Qs Jc 10d]", created.toString());
	}

	@Test
	public void straightBestHand() {
		IEvaluator s = new StraightEvaluator(4, 5, 13);
		Hand created = s.getBestHand(AhKdQsJc10d);
		String msg = "The best hand for Ah, Kd, Qs, Jc, and 10d should be: ";
		assertEquals(msg, "Straight (4) [Ah Kd Qs Jc 10d]", created.toString());
	}

	@Test
	public void straightFlushSimple() {
		IEvaluator sf = new StraightFlushEvaluator(5, 5, 13);
		String msg = "Cards Kh, Qh, Jh, 10h, and 9h should satisfy a straight flush.";
		assertTrue(msg, sf.canSatisfy(KhQhJh10h9h));
	}

	@Test
	public void straightFlushOneWrongSuit() {
		IEvaluator sf = new StraightFlushEvaluator(5, 5, 13);
		Card[] cards = KhQhJh10h9h;
		cards[2] = new Card(11, Suit.DIAMONDS);
		String msg = "Cards Kh, Qh, Jd, 10h, and 9h should not satisfy a straight flush.";
		assertFalse(msg, sf.canSatisfy(cards));
	}

	@Test
	public void straightFlushCreateHand() {
		IEvaluator sf = new StraightFlushEvaluator(5, 5, 13);
		int[] subset = { 0, 1, 2, 3, 4 };
//		int[] subset = { 10, 9, 13, 12, 11};
		Hand created = sf.createHand(KhQhJh10h9h, subset);
		String msg = "The cards Kh, Qh, Jh, 10h, and 9h create a straight hand: ";
		assertEquals(msg, "Straight Flush (5) [Kh Qh Jh 10h 9h]",
				created.toString());
	}

	@Test
	public void fullHouseToString() {
		IEvaluator fh = new FullHouseEvaluator(3, 5);
		Arrays.sort(tend10c5s5h5c);
		Hand createdbest = fh.getBestHand(tend10c5s5h5c);
		String msg = "The best hand is: ";
		assertEquals(msg, "Full House (3) [5s 5h 5c 10d 10c]",
				createdbest.toString());
	}

	@Test
	public void fullHouseNullToString() {
		IEvaluator fh = new FullHouseEvaluator(3, 5);
		Arrays.sort(AhKdQsJc10d);
		Hand createdbest = fh.getBestHand(AhKdQsJc10d);
		String msg = "The best hand is null for Ah, Kd, Qs, Jc, and 10d: ";
		assertNull(msg, createdbest);
	}

	// second
	@Test
	public void onePairFromTwoPairs() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] mainCards = { JsJh6h4d4c[0], JsJh6h4d4c[1] };
		Card[] sideCards = { JsJh6h4d4c[2], JsJh6h4d4c[3], JsJh6h4d4c[4] };
		Hand created = new Hand(mainCards, sideCards, op);
		String msg = "A new Hand must be Jh and Jh as the main cards and 6h, 4d, and 4c as side cards: ";
		assertEquals(msg, created.toString(), "One Pair (2) [Js Jh : 6h 4d 4c]");
	}

	@Test
	public void onePairFromThreePairs() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] mainCards = { JsJh6h4d4c[0], JsJh6h4d4c[1] };
		Card[] sideCards = { new Card(6, Suit.DIAMONDS), JsJh6h4d4c[2],
				JsJh6h4d4c[3], JsJh6h4d4c[4] };
		Hand created = new Hand(mainCards, sideCards, op);
		String msg = "A new Hand must be Js and Jh as the main cards and 6d, 6h, and 4d as side cards: ";
		assertEquals(msg, created.toString(), "One Pair (2) [Js Jh : 6d 6h 4d]");
	}

	@Test
	public void onePairFromTwoPairsMiddle() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] mainCards = { new Card(6, Suit.DIAMONDS), JsJh6h4d4c[2] };
		Card[] sideCards = { JsJh6h4d4c[0], JsJh6h4d4c[3], JsJh6h4d4c[4] };
		Hand created = new Hand(mainCards, sideCards, op);
		String msg = "A new Hand must be 6d and 6h as the main cards and Js, 4d, and 4c as side cards: ";
		assertEquals(msg, created.toString(), "One Pair (2) [6d 6h : Js 4d 4c]");
	}

	@Test
	public void bestOnePairFromTwoPairs() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] mainCards = { JsJh6h4d4c[0], JsJh6h4d4c[1] };
		Card[] sideCards = { JsJh6h4d4c[2], JsJh6h4d4c[3], JsJh6h4d4c[4] };
		Hand created = new Hand(mainCards, sideCards, op);
		Hand best = op.getBestHand(JsJh6h4d4c);
		String msg = "The best Hand must be Js and Jh as the main cards and 6h, 4d, and 4c as side cards: ";
		assertEquals(msg, created.toString(), best.toString());
	}

	@Test
	public void TresFromTwoTriples() {
		IEvaluator op = new ThreeOfAKindEvaluator(2, 5);
		Card[] mainCards = { JsJhJc6h4s4d4c[0], JsJhJc6h4s4d4c[1],
				JsJhJc6h4s4d4c[2] };
		Card[] sideCards = { JsJhJc6h4s4d4c[3], JsJhJc6h4s4d4c[4],
				JsJhJc6h4s4d4c[5], JsJhJc6h4s4d4c[6] };
		Hand created = new Hand(mainCards, sideCards, op);
		String msg = "A new Hand must be Js, Jh, and Jc as the main cards and 6h and 4d as side cards: ";
		assertEquals(msg, created.toString(),
				"Three of a Kind (2) [Js Jh Jc : 6h 4s]");
	}

	@Test
	public void TresFromOneTriple() {
		IEvaluator tk = new ThreeOfAKindEvaluator(2, 5);
		Card[] mainCards = { JsJhJc6h4s4d4c[4], JsJhJc6h4s4d4c[5],
				JsJhJc6h4s4d4c[6] };
		Card[] sideCards = { JsJhJc6h4s4d4c[1], JsJhJc6h4s4d4c[2],
				JsJhJc6h4s4d4c[3] };
		Hand created = new Hand(mainCards, sideCards, tk);
		String msg = "A new Hand must be 4s, 4d, and 4c as the main cards and Jh and Jc as side cards: ";
		assertEquals(msg, created.toString(),
				"Three of a Kind (2) [4s 4d 4c : Jh Jc]");
	}

	@Test
	public void bestTresFromTwoTriples() {
		IEvaluator tk = new ThreeOfAKindEvaluator(2, 5);
		Card[] mainCards = { JsJhJc6h4s4d4c[0], JsJhJc6h4s4d4c[1],
				JsJhJc6h4s4d4c[2] };
		Card[] sideCards = { JsJhJc6h4s4d4c[3], JsJhJc6h4s4d4c[4],
				JsJhJc6h4s4d4c[5], JsJhJc6h4s4d4c[6] };
		Hand created = new Hand(mainCards, sideCards, tk);
		Hand best = tk.getBestHand(JsJhJc6h4s4d4c);
		String msg = "The best Hand must be Js, Jh, and Jc as the main cards and 6h and 4s as side cards: ";
		assertEquals(msg, created.toString(), best.toString());
	}

	@Test
	public void fullHouseOddHand() {
		IEvaluator fh = new FullHouseEvaluator(4, 5);
		Hand created = new Hand(JsJhJd3s3c, null, fh);
		String msg = "A full house of Js, Jh, Jd, 3s, and 3c:";
		assertEquals(msg, "Full House (4) [Js Jh Jd 3s 3c]", created.toString());
	}

	@Test
	public void bestFullHouseOddHand() {
		IEvaluator fh = new FullHouseEvaluator(4, 5);
		Hand created = new Hand(JsJhJd3s3c, null, fh);
		Hand best = fh.getBestHand(JsJhJd3s3c);
		String msg = "A full house of Js, Jh, Jd, 3s, and 3c:";
		assertEquals(msg, created.toString(), best.toString());
	}

	@Test
	public void fullHouseEvenHand() {
		IEvaluator fh = new FullHouseEvaluator(4, 4);
		Card[] JsJh3s3c = { JsJhJd3s3c[0], JsJhJd3s3c[1], JsJhJd3s3c[3],
				JsJhJd3s3c[4] };
		Hand created = new Hand(JsJh3s3c, null, fh);
		String msg = "A full house of Js, Jh, 3s, and 3c:";
		assertEquals(msg, "Full House (4) [Js Jh 3s 3c]", created.toString());
	}

	@Test
	public void fullHouseOddHandBigSetSecond() {
		IEvaluator fh = new FullHouseEvaluator(4, 5);
		Card[] JsJh3s3d3c = { JsJhJd3s3c[0], JsJhJd3s3c[1], JsJhJd3s3c[3],
				new Card(3, Suit.DIAMONDS), JsJhJd3s3c[4] };
		int[] subset = { 0, 1, 2, 3, 4 };
		Hand created = fh.createHand(JsJh3s3d3c, subset);
		String msg = "A full house of 3s, 3d, 3c,  Js, and Jh:";
		assertEquals(msg, "Full House (4) [3s 3d 3c Js Jh]", created.toString());
	}

	// first
	@Test
	public void onePairSimple() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] main = { AsAdKh7c5d[0], AsAdKh7c5d[1] };
		String msg = "A new one pair evaluator should satisfy sorted main cards of As and Ad: ";
		assertTrue(msg, op.canSatisfy(main));
	}

//	@Test
//	public void onePairUnsorted() {
//		IEvaluator op = new OnePairEvaluator(2, 5);
//		Card[] main = { AsAdKh7c5d[1], AsAdKh7c5d[0] };
//		String msg = "A new one pair evaluator should not be satisfied by unsorted main cards of Ad and As: ";
//		assertFalse(msg, op.canSatisfy(main));
//	}

	@Test
	public void onePairNotAPair() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] main = { AsAdKh7c5d[0], AsAdKh7c5d[2] };
		String msg = "A new one pair evaluator should not be satisfied by main cards of As and Kh: ";
		assertFalse(msg, op.canSatisfy(main));
	}

	@Test
	public void onePairUnsortedNotPair() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] main = { AsAdKh7c5d[4], AsAdKh7c5d[2] };
		String msg = "A new one pair evaluator should not be satisfied by unsorted main cards of 5d and Kh: ";
		assertFalse(msg, op.canSatisfy(main));
	}

	@Test
	public void onePairTooManyCards() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] main = { AsAdKh7c5d[0], AsAdKh7c5d[1], AsAdKh7c5d[2] };
		String msg = "A new one pair evaluator should not be satisfied by sorted main cards of As, Ad, and Kh: ";
		assertFalse(msg, op.canSatisfy(main));
	}

	@Test
	public void onePairCreateHand1() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] all = AsAdKh7c5d;
		Card[] main = { AsAdKh7c5d[0], AsAdKh7c5d[1] };
		Card[] side = { AsAdKh7c5d[2], AsAdKh7c5d[3], AsAdKh7c5d[4] };
		int[] subset = { 0, 1 };
		Hand same = new Hand(Arrays.copyOf(main, main.length), Arrays.copyOf(
				side, side.length), op);
		String msg = "A hand created using cards As, Ad, Kh, 7c, and 5d AND the subset 0, 1 should work: ";
		Hand created = op.createHand(all, subset);
		assertEquals(msg, 0, created.compareTo(same));
	}

	// fixed 4/26
	@Test
	public void onePairCreateBestHand1() {
		IEvaluator op = new OnePairEvaluator(2, 5);
		Card[] all = AsAdKh5c5d;
		all[3] = all[4];
		all[4] = new Card(5, Suit.CLUBS);
		String msg = "The best hand created using cards As, Ad, Kh, 5d, and 5c should be: ";
		assertEquals(msg, "One Pair (2) [As Ad : Kh 5d 5c]", op
				.getBestHand(all).toString());
	}

	@Test
	public void catchAllSimple() {
		IEvaluator ca = new CatchAllEvaluator(0, 5);
		Card[] all = AsAdKh7c5d;
		String msg = "A new catch-all evaluator with hand size 5 should be satisfied by sorted cards As, Ad, Kh, 7c, and 5d: ";
		assertTrue(msg, ca.canSatisfy(all));
	}

//	@Test
//	public void catchAllUnsorted1() {
//		IEvaluator ca = new CatchAllEvaluator(0, 5);
//		Card[] all = AsAdKh7c5d;
//		Card temp = all[0];
//		all[0] = all[4];
//		all[4] = temp;
//		String msg = "A new catch-all evaluator of hand size 5 should not be satified by unsorted cards 5d, Ad, Kh, 7c, and As: ";
//		assertFalse(msg, ca.canSatisfy(all));
//	}
//
//	@Test
//	public void catchAllUnsorted2() {
//		IEvaluator ca = new CatchAllEvaluator(0, 5);
//		Card[] all = AsAdKh7c5d;
//		Card temp = all[0];
//		all[0] = all[1];
//		all[1] = temp;
//		String msg = "A new catch-all evaluator of hand size 5 should not be satified by the subtley unsorted cards Ad, As, Kh, 7c, and 5d: ";
//		assertFalse(msg, ca.canSatisfy(all));
//	}

	@Test
	public void catchAllTooFewCards() {
		IEvaluator ca = new CatchAllEvaluator(0, 5);
		Card[] all = new Card[4];
		for (int i = 0; i < 4; i++)
			all[i] = AsAdKh7c5d[i];
		String msg = "A new catch-all evaluator with hand size 5 should not be satisfied by sorted cards As, Ad, Kh, and 7c: ";
		assertFalse(msg, ca.canSatisfy(all));
	}

	@Test
	public void allPrimesSimple() {
		IEvaluator ap = new AllPrimesEvaluator(1, 5);
		Card[] all = AsAdKh7c5d;
		all[0] = new Card(23, Suit.SPADES);// 961748927, Suit.SPADES);
		all[1] = new Card(17, Suit.DIAMONDS);// 817504253, Suit.DIAMONDS);
		String msg = "A new all-primes evaluator should be satisfied by sorted cards 961748927s, 817504253d, Kh, 7c, and 5d: ";
		assertTrue(msg, ap.canSatisfy(all));
	}

	@Test
	public void allPrimesUnsorted() {
		IEvaluator ap = new AllPrimesEvaluator(1, 5);
		Card[] all = AsAdKh7c5d;
		Card temp = all[0];
		all[0] = all[4];
		all[4] = temp;
		String msg = "A new all-primes evaluator of hand size 5 should not be satified by unsorted cards 5d, Ad, Kh, 7c, and As: ";
		assertFalse(msg, ap.canSatisfy(all));
	}

	@Test
	public void allPrimesTooFewCards() {
		IEvaluator ap = new AllPrimesEvaluator(1, 5);
		Card[] all = new Card[4];
		for (int i = 0; i < 4; i++)
			all[i] = AsAdKh7c5d[i];
		String msg = "A new all-primes evaluator with hand size 5 should not be satisfied by sorted cards As, Ad, Kh, and 7c: ";
		assertFalse(msg, ap.canSatisfy(all));
	}

	@Test
	public void allPrimesNotPrimes() {
		IEvaluator ap = new AllPrimesEvaluator(1, 5);
		Card[] all = Ah10c9s8d7c;
		String msg = "A new all-primes evaluator should not be satisfied by sorted cards Ah, 10c, 9s, 8d, and 7c: ";
		assertFalse(msg, ap.canSatisfy(all));
	}

	@Test
	public void tresSimple() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[1], Ah9s9h9d7c[2], Ah9s9h9d7c[3] };
		String msg = "A new three of a kind evaluator should satisfy sorted main cards of 9s, 9h, and 9d: ";
		assertTrue(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresNotSame() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[0], Ah9s9h9d7c[2], Ah9s9h9d7c[3] };
		String msg = "A new three of a kind evaluator should not satisfy sorted main cards of Ah, 9h, and 9d: ";
		assertFalse(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresNotSame2() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[1], Ah9s9h9d7c[2], Ah9s9h9d7c[4] };
		String msg = "A new three of a kind evaluator should not satisfy sorted main cards of 9s, 9h, and 7c: ";
		assertFalse(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresNotSame3() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[0], Ah9s9h9d7c[2], Ah9s9h9d7c[4] };
		String msg = "A new three of a kind evaluator should not satisfy sorted main cards of Ah, 9h, and 7c: ";
		assertFalse(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresTooManyCards1() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[1], Ah9s9h9d7c[2], Ah9s9h9d7c[3],
				Ah9s9h9d7c[4] };
		String msg = "A new three of a kind evaluator should not satisfy sorted main cards of 9s, 9h, 9d, and 7c: ";
		assertFalse(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresTooManyCards2() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[0], Ah9s9h9d7c[1], Ah9s9h9d7c[2],
				Ah9s9h9d7c[3], Ah9s9h9d7c[4] };
		String msg = "A new three of a kind evaluator should not satisfy sorted main cards of Ah, 9s, 9h, 9d, and 7c: ";
		assertFalse(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresTooFewCards1() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[1], Ah9s9h9d7c[2] };
		String msg = "A new three of a kind evaluator should not satisfy sorted main cards of 9s and 9h: ";
		assertFalse(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresTooFewCards2() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[1], Ah9s9h9d7c[4] };
		String msg = "A new three of a kind evaluator should not satisfy sorted main cards of 9s and 7c: ";
		assertFalse(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresTooFewCards3() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] main = { Ah9s9h9d7c[1] };
		String msg = "A new three of a kind evaluator should not satisfy sorted main cards of 9s: ";
		assertFalse(msg, tk.canSatisfy(main));
	}

	@Test
	public void tresCreateHand1() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d7c;
		Card[] main = { Ah9s9h9d7c[1], Ah9s9h9d7c[2], Ah9s9h9d7c[3] };
		Card[] side = { Ah9s9h9d7c[0], Ah9s9h9d7c[4] };
		int[] subset = { 1, 2, 3 };
		Hand same = new Hand(main, side, tk);
		String msg = "A hand created using cards Ah, 9s, 9h, 9d, and 7c AND the subset 1, 2, 3 should work: ";
		assertEquals(msg, 0, tk.createHand(all, subset).compareTo(same));
	}

	@Test
	public void tresCreateBestHand1() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d7c;
		Card[] main = { Ah9s9h9d7c[1], Ah9s9h9d7c[2], Ah9s9h9d7c[3] };
		Card[] side = { Ah9s9h9d7c[0], Ah9s9h9d7c[4] };
		Hand best = new Hand(main, side, tk);
		String msg = "The best hand created using cards Ah, 9s, 9h, 9d, and 7c should be "
				+ best.toString() + ":";
		assertEquals(msg, 0, tk.getBestHand(all).compareTo(best));
	}

	@Test
	public void tresCreateBestHand2() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d9c;
		Card[] main = { Ah9s9h9d9c[1], Ah9s9h9d9c[2], Ah9s9h9d9c[3] };
		Card[] side = { Ah9s9h9d9c[0], Ah9s9h9d9c[4] };
		Hand best = new Hand(main, side, tk);
		String msg = "The best hand created using cards Ah, 9s, 9h, 9d, and 7c should be "
				+ best.toString() + ":";
		assertEquals(msg, 0, tk.getBestHand(all).compareTo(best));
	}

	@Test
	public void tresCreateNullHand1() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d7c;
		int[] subset = { 0, 2, 3 };
		String msg = "A hand created using cards Ah, 9s, 9h, 9d, and 7c AND the subset 0, 2, 3 should be null: ";
		assertNull(msg, tk.createHand(all, subset));
	}

	@Test
	public void tresCreateNullHand2() {
		IEvaluator tk = new ThreeOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d7c;
		int[] subset = { 2, 3 };
		String msg = "A hand created using cards Ah, 9s, 9h, 9d, and 7c AND the subset 2, 3 should be null: ";
		assertNull(msg, tk.createHand(all, subset));
	}

	@Test
	public void cuatroSimple() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[1], Ah9s9h9d9c[2], Ah9s9h9d9c[3],
				Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should satisfy sorted main cards of 9s, 9h, 9d, and 9c: ";
		assertTrue(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroNotSame1() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[0], Ah9s9h9d9c[2], Ah9s9h9d9c[3],
				Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should not satisfy sorted main cards of Ah, 9h, 9d, and 9c: ";
		assertFalse(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroNotSame2() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[0], new Card(1, Suit.SPADES), Ah9s9h9d9c[3],
				Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should not satisfy sorted main cards of Ah, As, 9d, and 9c: ";
		assertFalse(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroNotSame3() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[0], new Card(12, Suit.SPADES),
				Ah9s9h9d9c[3], Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should not satisfy sorted main cards of Ah, As, 9d, and 9c: ";
		assertFalse(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroTooMany() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[0], Ah9s9h9d9c[1], Ah9s9h9d9c[2],
				Ah9s9h9d9c[3], Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should not satisfy sorted main cards of Ah, 9s, 9h, 9d, and 9c: ";
		assertFalse(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroTooFew1() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[2], Ah9s9h9d9c[3], Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should not satisfy sorted main cards of 9h, 9d, and 9c: ";
		assertFalse(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroTooFew2() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[0], Ah9s9h9d9c[3], Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should not satisfy sorted main cards of Ah, 9d, and 9c: ";
		assertFalse(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroTooFew3() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[3], Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should not satisfy sorted main cards of 9d and 9c: ";
		assertFalse(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroTooFew4() {
		IEvaluator fk = new FourOfAKindEvaluator(4, 5);
		Card[] main = { Ah9s9h9d9c[4] };
		String msg = "A new four of a kind evaluator should not satisfy sorted main cards of 9c: ";
		assertFalse(msg, fk.canSatisfy(main));
	}

	@Test
	public void cuatroCreateNullHand1() {
		IEvaluator fk = new FourOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d9c;
		int[] subset = { 0, 2, 3, 4 };
		String msg = "A hand created using cards Ah, 9s, 9h, 9d, and 7c AND the subset 0, 2, 3, 4 should be null: ";
		assertNull(msg, fk.createHand(all, subset));
	}

	@Test
	public void cuatroCreateNullHand2() {
		IEvaluator fk = new FourOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d9c;
		int[] subset = { 2, 3, 4 };
		String msg = "A hand created using cards Ah, 9s, 9h, 9d, and 7c AND the subset 2, 3, 4 should be null: ";
		assertNull(msg, fk.createHand(all, subset));
	}

	@Test
	public void cuatroCreateHand1() {
		IEvaluator fk = new FourOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d9c;
		Card[] main = { Ah9s9h9d9c[1], Ah9s9h9d9c[2], Ah9s9h9d9c[3],
				Ah9s9h9d9c[4] };
		Card[] side = { Ah9s9h9d9c[0] };
		int[] subset = { 1, 2, 3, 4 };
		Hand same = new Hand(main, side, fk);
		String msg = "A hand created using cards Ah, 9s, 9h, 9d, and 7c AND the subset 1, 2, 3, 4 should be legit: ";
		assertEquals(msg, 0, fk.createHand(all, subset).compareTo(same));
	}

	@Test
	public void cuatroBestCreateHand1() {
		IEvaluator fk = new FourOfAKindEvaluator(3, 5);
		Card[] all = Ah9s9h9d9c;
		Card[] main = { Ah9s9h9d9c[1], Ah9s9h9d9c[2], Ah9s9h9d9c[3],
				Ah9s9h9d9c[4] };
		Card[] side = { Ah9s9h9d9c[0] };
		Hand best = new Hand(main, side, fk);
		String msg = "A hand created using cards Ah, 9s, 9h, 9d, and 7c AND the subset 1, 2, 3, 4 should be legit: ";
		assertEquals(msg, best.toString(), fk.getBestHand(all).toString());
	}

	@Test
	public void fullSimple() {
		IEvaluator fh = new FullHouseEvaluator(5, 5);
		Card[] all = tend10c10h5s5c;
		String msg = "A new full house evaluator should be satisfied by 10d, 10h, 10c, 5s, and 5c: ";
		assertTrue(msg, fh.canSatisfy(all));
	}

	@Test
	public void fullBest() {
		IEvaluator fh = new FullHouseEvaluator(5, 5);
		Card[] all = tend10c10h5s5c;
		Hand created = fh.getBestHand(all);
		Hand best = new Hand(tend10c10h5s5c, null, fh);
		String msg = "The best fullhouse from 10d, 10h, 10c, 5s, and 5c is itself: ";
		assertEquals(msg, created.toString(), best.toString());
	}

	@Test
	public void fullSubset() {
		IEvaluator fh = new FullHouseEvaluator(5, 5);
		Card[] all = tend10c5s5h5c;
		Card[] sorted = new Card[all.length];
		for (int i = 0; i < 3; i++) {
			sorted[i] = all[i + 2];
			if (i == 2)
				break;
			sorted[i + 3] = all[i];
		}
		int[] subset = { 0, 1, 2, 3, 4 };
//		int[] subset = { 10, 5};
		Hand fhCreated = fh.createHand(all, subset);
		Hand manuallyCreated = new Hand(sorted, null, fh);
		String msg = "The cards 10d, 10c, 5s, 5h, and 5c should satisfy a full house even though they aren't ordered big small: ";
		assertEquals(msg, fhCreated.toString(), manuallyCreated.toString());
	}
	
	
}