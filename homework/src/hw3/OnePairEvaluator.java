package hw3;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import api.Card;
import api.Hand;
import api.IEvaluator;
import util.SubsetFinder;

/**
 * Evaluator for a hand containing (at least) two cards of the same rank. The
 * number of cards required is two.
 * 
 * The name of this evaluator is "One Pair".
 */
public class OnePairEvaluator extends AbstractEvaluator {

	/**
	 * Constructs the evaluator.
	 * 
	 * @param ranking
	 *            ranking of this hand
	 * @param handSize
	 *            number of cards in a hand
	 */
	public OnePairEvaluator(int ranking, int handSize) {
		super();
		super.ranking = ranking;
		super.handSize = handSize;
		super.numMainCards = 2;
		super.name = "One Pair";

	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public int getRanking() {
		return super.getRanking();
	}

	@Override
	public int cardsRequired() {
		return super.cardsRequired();
	}

	@Override
	public int handSize() {
		return super.handSize();
	}

	@Override
	public boolean canSatisfy(Card[] mainCards) {
		if (mainCards.length != this.cardsRequired()) {
			return false;
		} else {
			if (mainCards[0].getRank() == mainCards[1].getRank()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean canSubsetSatisfy(Card[] allCards) {
		return super.canSubsetSatisfy(allCards);
	}

	@Override
	public Hand createHand(Card[] allCards, int[] subset) {
		return super.createHand(allCards, subset);
	}


	@Override
	public Hand getBestHand(Card[] allCards) {
		return super.getBestHand(allCards);

	}

}
