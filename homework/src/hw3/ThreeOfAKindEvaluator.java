package hw3;

import java.util.ArrayList;

import api.Card;
import api.Hand;

/**
 * Evaluator for a hand containing (at least) three cards of the same rank. The
 * number of cards required is three.
 * 
 * The name of this evaluator is "Three of a Kind".
 */
public class ThreeOfAKindEvaluator extends AbstractEvaluator {
	/**
	 * Constructs the evaluator.
	 * 
	 * @param ranking
	 *            ranking of this hand
	 * @param handSize
	 *            number of cards in a hand
	 */
	public ThreeOfAKindEvaluator(int ranking, int handSize) {
		super();
		super.ranking = ranking;
		super.handSize = handSize;
		super.numMainCards = 3;
		super.name = "Three of a Kind";
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public int getRanking() {
		return super.getRanking();
	}

	@Override
	public int cardsRequired() {
		return super.cardsRequired();
	}

	@Override
	public int handSize() {
		return super.handSize();
	}

	@Override
	public boolean canSatisfy(Card[] mainCards) {
		return super.mainCardsSameKind(mainCards);
	}

	@Override
	public boolean canSubsetSatisfy(Card[] allCards) {
		return super.canSubsetSatisfy(allCards);
	}

	@Override
	public Hand createHand(Card[] allCards, int[] subset) {
		return super.createHand(allCards, subset);
	}

	@Override
	public Hand getBestHand(Card[] allCards) {
		return super.getBestHand(allCards);
	}

}
