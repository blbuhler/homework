package hw3;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import api.Card;
import api.Hand;
import api.IEvaluator;

public class EvaluatorTest {

	IEvaluator onePairEval;
	IEvaluator threeKindEval;
	IEvaluator catchEval;
	IEvaluator allPrimeEval;

	@Before
	public void setup() {
		onePairEval = new OnePairEvaluator(3, 4);
		threeKindEval = new ThreeOfAKindEvaluator(2, 5);
		catchEval = new CatchAllEvaluator(2, 5);
		allPrimeEval = new AllPrimesEvaluator(2, 5);
	}

	// For One Pair Evaluator
	@Test
	public void checkInitialName() {
		String msg = ("The name for the One Pair Evaluator should be One Pair");
		assertEquals(msg, "One Pair", onePairEval.getName());
	}

	@Test
	public void checkRankingAndHandSize() {
		String msg = ("The newly constructed OnePairEvaluator(3, 4) should have ranking of 3 and hand size of 4");
		assertEquals(msg, 3, onePairEval.getRanking());
		assertEquals(msg, 4, onePairEval.handSize());
	}

	@Test
	public void checkSameRankingDifferentSuitsCanSatisfy() {
		Card[] cards = Card.createArray("2c, 2d");
		String msg = ("The newly constructed cards[2c, 2d] should satisfy the one pair evaluator");
		assertEquals(msg, true, onePairEval.canSatisfy(cards));
	}

	@Test
	public void checkDifferentRankingCanSatisfy() {
		Card[] cards = Card.createArray("Kc, Qd");
		String msg = ("The newly constructed cards[Kc, Qd] should not satisfy the one pair evaluator");
		assertEquals(msg, false, onePairEval.canSatisfy(cards));
	}

	@Test
	public void checkMainCardsMoreThanRequiredCanSatisfy() {
		Card[] cards = Card.createArray("2c, 2d, 3h");
		String msg = ("The newly constructed cards[2c, 2d, 3h] should not satisfy the one pair evaluator");
		assertEquals(msg, false, onePairEval.canSatisfy(cards));
	}

	@Test
	public void checkAllCardsSubsetCanSatisfy() {
		Card[] cards = Card.createArray("3h, 2c, 2d,");
		String msg = ("The newly constructed cards[2c, 2d, 3h]'s subset should satisfy the one pair evaluator");
		assertEquals(msg, true, onePairEval.canSubsetSatisfy(cards));
	}

	@Test
	public void checkAllCardsSubsetCanSatisfy2() {
		Card[] cards = Card.createArray("6s, Jh, Ah, 10h, 6h, Js, 6c, Kh, Qh");
		Arrays.sort(cards);
		String msg = ("The newly constructed cards[6s, Jh, Ah, 10h, 6h, Js, 6c, Kh, Qh]'s subset should satisfy the one pair evaluator");
		assertEquals(msg, true, onePairEval.canSubsetSatisfy(cards));
	}

	@Test
	public void checkCreateHand() {
		Card[] cards = Card.createArray("6s, Jh, Ah, 10h, 6h, Js, 6c, Kh, Qh");
		Arrays.sort(cards);
		int[] subset = { 6, 8 };
		Hand hand = onePairEval.createHand(cards, subset);
		String msg = ("The newly constructed cards[2c, 2d, 3h]'s subset should satisfy the one pair evaluator");
		assertEquals(msg, "One Pair (3) [6s 6c : Ah Kh]", hand.toString());
	}

	@Test
	public void checkGetBestHand() {
		Card[] cards = Card.createArray("6s, Jh, Ah, 10h, 6h, Js, 6c, Kh, Qh");
		Arrays.sort(cards);
		String msg = ("The newly constructed cards[6s, Jh, Ah, 10h, 6h, Js, 6c, Kh, Qh]'s best hand should be One Pair (3) [Js Jh: Ah Kh]");
		assertEquals(msg, "One Pair (3) [Js Jh : Ah Kh]", onePairEval
				.getBestHand(cards).toString());
	}

	@Test
	public void evaluatorsFiveCards() {
		Card[] cards = Card.createArray("6s, Jh, Ah, 10h, 6h, Js, 6c, Kh, Qh");
		Arrays.sort(cards);
		ArrayList<IEvaluator> evaluators = new ArrayList<IEvaluator>();
		evaluators.add(new OnePairEvaluator(3, 5));
		evaluators.add(new FullHouseEvaluator(1, 5));
		evaluators.add(new StraightEvaluator(2, 5, 13));
		Hand best = null;
		for (IEvaluator e : evaluators) {
			Hand h = e.getBestHand(cards);
			if (best == null || h != null && h.compareTo(best) < 0) {
				best = h;
			}
		}
		String msg = "Best Hand";
		assertEquals(msg, "Full House (1) [6s 6h 6c Js Jh]", best.toString());
	}
	
	@Test
	public void fullHouseCanSatisfy(){
		Card[] cards = Card.createArray("Ah, 10h, 6s, 6h, 6c");
		Arrays.sort(cards);
		String msg = "Cannot satisfy";
		IEvaluator fullHouseE = new FullHouseEvaluator(1, 5);
		assertEquals(msg, false, fullHouseE.canSatisfy(cards));
	}
	
	@Test
	public void straightAceLowCanSatisfy(){
		Card[] cards = Card.createArray("Ah, 5h, 4s, 3s, 2s");
		Arrays.sort(cards);
		String msg = "Ah, 5h, 4s, 3s, 2s can satisfy";
		IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 5);
		assertEquals(msg, true, StraightEvaluator.canSatisfy(cards));
	}
	
	@Test
	public void straightAceLowCreateHand(){
		Card[] cards = Card.createArray("Ah, 5h, 4s, 3s, 2s, 3s, 9s");
		Arrays.sort(cards);
		String msg = "Ah, 5h, 4s, 3s, 2s, 3s, 9s best hand";
		IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 5);
		assertEquals(msg, "Straight (1) [5h 4s 3s 2s Ah]", StraightEvaluator.getBestHand(cards).toString());
	}
	
	@Test
	public void straightAceHighCanNotSatisfy(){
		Card[] cards = Card.createArray("Ah, 6h, 5s, 4s, 3c");
		Arrays.sort(cards);
		String msg = "Ah, 6h, 5s, 4s, 3c cannot satisfy";
		IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 7);
		assertEquals(msg, false, StraightEvaluator.canSatisfy(cards));
	}
	
	@Test
	public void straightAceHighCanNotCreateHand(){
		Card[] cards = Card.createArray("Ah, 6h, 5s, 4s, 3c");
		Arrays.sort(cards);
		String msg = "Ah, 6h, 5s, 4s, 3c cannot create hand";
		IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 7);
		assertEquals(msg, null, StraightEvaluator.getBestHand(cards));
	}
	
	@Test
	public void straightAceHighCanSatisfy(){
		Card[] cards = Card.createArray("Ah, 6h, 5s, 4s, 3c");
		Arrays.sort(cards);
		String msg = "Ah, 6h, 5s, 4s, 3c cannot satisfy";
		IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 6);
		assertEquals(msg, true, StraightEvaluator.canSatisfy(cards));
	}
	
	@Test
	public void straightAceHighCreateHand(){
		Card[] cards = Card.createArray("Ah, Qh, Js, 10s, Kc");
		Arrays.sort(cards);
		String msg = "Ah, Qh, Js, 10s, Kc should make Ace comes before";
		IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 13);
		assertEquals(msg, "Straight (1) [Ah Kc Qh Js 10s]", StraightEvaluator.getBestHand(cards).toString());
	}
	
	@Test
	public void multipleCanSatisfy(){
		Card[] cards = Card.createArray("5s, 5d, 5c, 7h, 11s");
		Arrays.sort(cards);
		String msg = "5s, 5d, 5c, 7h, 11s can satisfy Three of a Kind, One Pair, Catch All, and AllPrimes";
		assertEquals(msg, true, threeKindEval.canSubsetSatisfy(cards));
		assertEquals(msg, true, catchEval.canSubsetSatisfy(cards));
		assertEquals(msg, true, allPrimeEval.canSubsetSatisfy(cards));
		assertEquals(msg, true, onePairEval.canSubsetSatisfy(cards));
	}
	
	@Test
	public void straightBestHandWithoutAce(){
		IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 8);
		Card[] cards = Card.createArray("Ah, 6s, 5d, 4s, 3s, 2c");
		Arrays.sort(cards);
		String msg = "Ah, 6s, 5d, 4s, 3s, 2c's best hand";
		assertEquals(msg, "Straight (1) [6s 5d 4s 3s 2c]", StraightEvaluator.getBestHand(cards).toString());
	}
	
	@Test
	public void fullHouseBestHand(){
		IEvaluator StraightEvaluator = new FullHouseEvaluator(1, 5);
		Card[] cards = Card.createArray("Kh, Ks, 5d, 5s, 5s, 4c, 4s, 4d, 2c, 2s");
		Arrays.sort(cards);
		String msg = "Ah, 6s, 5d, 4s, 3s, 2c's best hand";
		assertEquals(msg, "Full House (1) [5s 5s 5d Ks Kh]", StraightEvaluator.getBestHand(cards).toString());
	}
}
