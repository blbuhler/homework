package hw3;

import java.util.ArrayList;

import api.Card;
import api.Hand;

/**
 * Evaluator for a hand consisting of a "straight" in which the card ranks are
 * consecutive numbers. The number of required cards is equal to the hand size.
 * An ace (card of rank 1) may be treated as the highest possible card or as the
 * lowest (not both). To evaluate a straight containing an ace it is necessary
 * to know what the highest card rank will be in a given game; therefore, this
 * value must be specified when the evaluator is constructed. In a hand created
 * by this evaluator the cards are listed in descending order with high card
 * first, e.g. [10 9 8 7 6] or [A K Q J 10], with one exception: In case of an
 * ace-low straight, the ace must appear last, as in [5 4 3 2 A]
 * 
 * The name of this evaluator is "Straight".
 */
public class StraightEvaluator extends AbstractEvaluator {
	private int maxCardRank;

	/**
	 * Constructs the evaluator. Note that the maximum rank of the cards to be
	 * used must be specified in order to correctly evaluate a straight with ace
	 * high.
	 * 
	 * @param ranking
	 *            ranking of this hand
	 * @param handSize
	 *            number of cards in a hand
	 * @param maxCardRank
	 *            largest rank of any card to be used
	 */
	public StraightEvaluator(int ranking, int handSize, int maxCardRank) {
		super();
		super.ranking = ranking;
		super.handSize = handSize;
		super.numMainCards = handSize;
		super.name = "Straight";
		this.maxCardRank = maxCardRank;

	}

	@Override
	public boolean canSatisfy(Card[] mainCards) {
		// Does the array have the same amount as cardsRequired?
		if (mainCards.length != this.cardsRequired()) {
			return false;
		} else {
			// Convert it ArrayList
			ArrayList<Card> mainCardsList = new ArrayList<Card>();
			super.cardsListGen(mainCards, mainCardsList);

			// If we find an Ace, remove it and calculate the difference between
			// the adjacent cards of remaining cards
			boolean hasAce = super.findRanks(mainCards, new int[] { 1 });

			if (hasAce) {
				// If we have an Ace, but the case is neither Ace Low nor Ace
				// High
				// Like handSize = 5, maxCardRank = 7, [A 6 5 4 3]
				// It is Straight but cannot satisfy the current evaluator
				if (!(isAceLow(mainCardsList) || isAceHigh(mainCardsList))) {
					return false;
				} else {
					mainCardsList.remove(0);
				}
			}

			// Get the rank of first card in cards without ace
			if (mainCardsList.get(0).getRank() > this.maxCardRank) {
				return false;
			}

			return cardsRankDiffIsOne(mainCardsList);
		}
	}

	private boolean cardsRankDiffIsOne(ArrayList<Card> mainCards) {
		int index = 0;
		boolean isDiffOne = true;
		while (index < mainCards.size() - 1 && isDiffOne) {
			boolean twoCardsDiffIsOne = (mainCards.get(index).getRank() - mainCards
					.get(index + 1).getRank()) == 1;
			if (!twoCardsDiffIsOne) {
				isDiffOne = false;
				return isDiffOne;
			}
			index++;
		}
		return isDiffOne;
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public int getRanking() {
		return super.getRanking();
	}

	@Override
	public int cardsRequired() {
		return super.cardsRequired();
	}

	@Override
	public int handSize() {
		return super.handSize();
	}

	@Override
	public boolean canSubsetSatisfy(Card[] allCards) {
		return super.canSubsetSatisfy(allCards);
	}

	@Override
	public Hand createHandByRank(Card[] allCards, int[] ranks) {
		ArrayList<Card> allCardsList = new ArrayList<Card>();
		super.cardsListGen(allCards, allCardsList);

		boolean allCardsHasAce = super.findRanks(allCards, new int[] { 1 });

		if (allCardsHasAce) {

			// First Find best hand that do not contain Ace
			Hand currentHandWithoutAce = findHandWithoutAce(ranks, allCardsList);

			// Then Find best hand that contain Ace

			Hand currentHandWithAce = findHandWithAce(allCards, ranks);

			// Sometime we can not find satisfied ranks without Ace from
			// allCards
			if (currentHandWithoutAce == null) {
				return currentHandWithAce;
			}

			// Compare them
			if (currentHandWithAce.compareTo(currentHandWithoutAce) < 0) {
				return currentHandWithAce;
			} else {
				return currentHandWithoutAce;
			}
		} else {
			return super.createHand(allCards, ranks);
		}
	}

	public Hand createHand(Card[] allCards, int[] subset) {
		ArrayList<Card> allCardsList = new ArrayList<Card>();
		super.cardsListGen(allCards, allCardsList);

		if (super.createHand(allCards, subset) == null) {
			return null;
		} else {
			Card[] mainCards = new Card[subset.length];

			for (int i = 0; i < mainCards.length; i++) {
				int indices = subset[i];
				mainCards[i] = allCards[indices];
			}
			if (super.findRanks(allCards, new int[] { 1 })) {
				ArrayList<Card> mainCardsList = new ArrayList<Card>();
				super.cardsListGen(mainCards, mainCardsList);

				if (isAceLow(mainCardsList)) {
					// Move it to the end of mainCards
					Card firstAceCard = mainCardsList.get(0);
					mainCardsList.remove(0);
					mainCardsList.add(firstAceCard);
				}

				// Convert mainCardsList to mainCards
				for (int i = 0; i < mainCardsList.size(); i++) {
					mainCards[i] = mainCardsList.get(i);
				}
			}

			if (canSatisfy(mainCards)) {
				return new Hand(mainCards, null, this);
			} else {
				return null;
			}
		}
	}

	/**
	 * Find the best hand with ace that contains cards of given ranks in a given
	 * array of all cards
	 * 
	 * @param allCards
	 *            All the cards
	 * @param ranks
	 *            Returned hand must contain at least one of given ranks
	 * @return The best hand with ace. return null if cards of given rank can be
	 *         found
	 */
	private Hand findHandWithAce(Card[] allCards, int[] ranks) {
		// currentHandWithAce
		Hand currentHandWithAce = super.createHandByRank(allCards, ranks);

		if (currentHandWithAce == null) {
			return null;
		}

		Card[] mainCards = currentHandWithAce.getMainCards();

		if (mainCards != null) {

			// MainCards to ArrayList
			ArrayList<Card> mainCardsList = new ArrayList<Card>();
			super.cardsListGen(mainCards, mainCardsList);

			// If mainCards has Ace
			boolean mainCardsHasAce = super.findRanks(mainCards,
					new int[] { 1 });
			if (mainCardsHasAce) {
				// Rearrange it in the Ace Low Case
				if (isAceLow(mainCardsList)) {
					// Move it to the end of mainCards
					Card firstAceCard = mainCardsList.get(0);
					mainCardsList.remove(0);
					mainCardsList.add(firstAceCard);
				}
			}

			// Convert mainCardsList to mainCards
			for (int i = 0; i < mainCardsList.size(); i++) {
				mainCards[i] = mainCardsList.get(i);
			}
		}

		currentHandWithAce = new Hand(mainCards, null, this);
		return currentHandWithAce;
	}

	/**
	 * Find best hand without ace by removing the fist ace card of the given
	 * cards list
	 * 
	 * @param ranks
	 *            Returned hand must contain at least one card of given ranks
	 * @param allCardsList
	 *            The cards to be picked from the all cards list
	 * @return the best hand without ace that contains cards of given ranks
	 */
	private Hand findHandWithoutAce(int[] ranks, ArrayList<Card> allCardsList) {
		allCardsList.remove(0);
		allCardsList.add(allCardsList.get(allCardsList.size() - 1));
		Card[] allCardsWithoutAce = new Card[allCardsList.size()];
		for (int i = 0; i < allCardsList.size(); i++) {
			allCardsWithoutAce[i] = allCardsList.get(i);
		}
		Hand currentHandWithoutAce = super.createHandByRank(allCardsWithoutAce,
				ranks);
		return currentHandWithoutAce;
	}

	/**
	 * Check if the card array list is Ace low case, where Ace should be put in
	 * the end of card list
	 * 
	 * @param mainCardsList
	 *            An sorted card array list
	 * @return true if the first card is Ace and the second card's rank is equal
	 *         to handSize, false otherwise
	 */
	private boolean isAceLow(ArrayList<Card> mainCardsList) {
		if (mainCardsList.get(0).getRank() == 1) {
			int secondCardRank = mainCardsList.get(1).getRank();
			// When handSize == maxCardRank, Ace low condition has the priority
			if (secondCardRank == handSize) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if the card array list is Ace High case, where Ace should not be
	 * moved
	 * 
	 * @param mainCardsList
	 *            An sorted card list array
	 * @return true if the first card is Ace and the second card's rank is equal
	 *         to maxCardRank, false otherwise
	 */
	private boolean isAceHigh(ArrayList<Card> mainCardsList) {
		if (mainCardsList.get(0).getRank() == 1) {
			int secondCardRank = mainCardsList.get(1).getRank();
			// When handSize == maxCardRank, Ace low condition has the priority
			if ((secondCardRank == maxCardRank) && !isAceLow(mainCardsList)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Hand getBestHand(Card[] allCards) {
		return super.getBestHand(allCards);
	}
}
