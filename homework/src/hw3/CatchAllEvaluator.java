package hw3;

import api.Card;
import api.Hand;

/**
 * Evaluator satisfied by any set of cards. The number of required cards is
 * equal to the hand size.
 * 
 * The name of this evaluator is "Catch All".
 */
public class CatchAllEvaluator extends AbstractEvaluator {
	/**
	 * Constructs the evaluator.
	 * 
	 * @param ranking
	 *            ranking of this hand
	 * @param handSize
	 *            number of cards in a hand
	 */
	public CatchAllEvaluator(int ranking, int handSize) {
		super();
		super.ranking = ranking;
		super.handSize = handSize;
		super.numMainCards = handSize;
		super.name = "Catch All";
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public int getRanking() {
		return super.getRanking();
	}

	@Override
	public int cardsRequired() {
		return super.cardsRequired();
	}

	@Override
	public int handSize() {
		return super.handSize();
	}

	@Override
	public boolean canSatisfy(Card[] mainCards) {
		if(mainCards.length  != cardsRequired()){
			return false;
		}
		return true;
	}

	

	@Override
	public boolean canSubsetSatisfy(Card[] allCards) {
		return super.canSubsetSatisfy(allCards);
	}

	@Override
	public Hand createHand(Card[] allCards, int[] subset) {
		return super.createHand(allCards, subset);
	}

	@Override
	public Hand getBestHand(Card[] allCards) {
		return super.getBestHand(allCards);
	}

}
