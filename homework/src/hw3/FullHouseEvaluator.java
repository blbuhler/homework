package hw3;

import java.util.ArrayList;
import java.util.Arrays;

import api.Card;
import api.Hand;
import api.Suit;

/**
 * Evaluator for a generalized full house. The number of required cards is equal
 * to the hand size. If the hand size is an odd number n, then there must be (n
 * / 2) + 1 cards of the matching rank and the remaining (n / 2) cards must be
 * of matching rank. In this case, when constructing a hand, the larger group
 * must be listed first even if of lower rank than the smaller group</strong>
 * (e.g. as [3 3 3 5 5] rather than [5 5 3 3 3]). If the hand size is even, then
 * half the cards must be of matching rank and the remaining half of matching
 * rank. Any group of cards, all of which are the same rank, always satisfies
 * this evaluator.
 * 
 * The name of this evaluator is "Full House".
 */
public class FullHouseEvaluator extends AbstractEvaluator {
	/**
	 * Constructs the evaluator.
	 * 
	 * @param ranking
	 *            ranking of this hand
	 * @param handSize
	 *            number of cards in a hand
	 */
	public FullHouseEvaluator(int ranking, int handSize) {
		super();
		super.ranking = ranking;
		super.handSize = handSize;
		super.numMainCards = handSize;
		super.name = "Full House";
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public int getRanking() {
		return super.getRanking();
	}

	@Override
	public int cardsRequired() {
		return super.cardsRequired();
	}

	@Override
	public int handSize() {
		return super.handSize();
	}

	@Override
	public boolean canSatisfy(Card[] mainCards) {
		ArrayList<Card> allMainCards = new ArrayList<Card>();
		super.cardsListGen(mainCards, allMainCards);

		// Check the length is same as cardsRequired
		if (mainCards.length != this.cardsRequired()) {
			return false;
		} else if (super.allSameKind(allMainCards)) {
			return true;
		} else {
			int midpoint;
			// Find the mid point of the cards
			// If the length mainCards is odd
			if (isOdd(mainCards.length)) {
				midpoint = (mainCards.length - 1) / 2;
			} else {
				midpoint = mainCards.length / 2 - 1;
			}

			// Left = [start, midpoint-1]
			ArrayList<Card> left = new ArrayList<Card>();
			for (int i = 0; i < midpoint; i++) {
				left.add(mainCards[i]);
			}
			boolean isLeftSameRanking = super.allSameKind(left);
			// Right = [midpoint+1, end]
			ArrayList<Card> right = new ArrayList<Card>();
			for (int i = midpoint + 1; i < mainCards.length; i++) {
				right.add(mainCards[i]);
			}
			boolean isRightSameRanking = super.allSameKind(right);

			if (isLeftSameRanking && isRightSameRanking) {
				left.add(mainCards[midpoint]);
				isRightSameRanking = super.allSameKind(right);
				isLeftSameRanking = super.allSameKind(left);
				if (!isLeftSameRanking) {
					// Move the midpoint card to the right array
					right.add(left.get(left.size() - 1));
					left.remove(left.size() - 1);
					isRightSameRanking = super.allSameKind(right);
					isLeftSameRanking = super.allSameKind(left);
				}
			}

			return isLeftSameRanking && isRightSameRanking;

		}

	}

	/**
	 * Check the given number is odd or even
	 * 
	 * @param number
	 *            A given number
	 * @return true if the given number is odd
	 */
	private boolean isOdd(int number) {
		int remainder = number % 2;
		return !(remainder == 0);
	}

	@Override
	public boolean canSubsetSatisfy(Card[] allCards) {
		return super.canSubsetSatisfy(allCards);
	}

	/**
	 * The same as createHandByRank in AbstractEvaluator, but in
	 * FullHouseEvaluator whose handSize is odd, the larger group should be put
	 * first
	 */
	@Override
	protected Hand createHandByRank(Card[] allCards, int[] ranks) {
		ArrayList<Card> allCardsList = new ArrayList<Card>();
		super.cardsListGen(allCards, allCardsList);

		if (isOdd(handSize)) {
			ArrayList<Card[]> subsetsCardsGroups = super.createSubsetsList(
					allCards, handSize);
			ArrayList<Hand> allPossibleHands = new ArrayList<Hand>();

			for (Card[] subsetCards : subsetsCardsGroups) {
				if (canSatisfy(subsetCards)
						&& super.findRanks(subsetCards, ranks)) {
					// Rearrange it and store it into allPossibleHand
					int midpoint = (subsetCards.length - 1) / 2;
					boolean rightLarger = subsetCards[midpoint].getRank() == subsetCards[subsetCards.length - 1]
							.getRank();
					Card[] possibleMainCards = Arrays.copyOf(subsetCards,
							subsetCards.length);
					if (rightLarger) {
						reverseCards(possibleMainCards);
					}
					Hand possibleHand = new Hand(possibleMainCards, null, this);
					allPossibleHands.add(possibleHand);
				}
			}

			if (allPossibleHands.size() == 0) {
				return null;
			} else {
				return findBestHand(allPossibleHands);
			}

		} else {
			return super.createHand(allCards, ranks);
		}
	}

	@Override
	public Hand createHand(Card[] allCards, int[] subset) {
		if (super.createHand(allCards, subset) == null) {
			return null;
		} else {
			Card[] mainCards = new Card[subset.length];
			for (int i = 0; i < mainCards.length; i++) {
				int indices = subset[i];
				mainCards[i] = allCards[indices];
			}

			if (isOdd(handSize)) {
				int midpoint = (mainCards.length - 1) / 2;
				boolean rightLarger = mainCards[midpoint].getRank() == mainCards[mainCards.length - 1]
						.getRank();
				if (rightLarger) {
					reverseCards(mainCards);
				}

				return new Hand(mainCards, null, this);
			}
			return new Hand(mainCards, null, this);
		}
	}

	/**
	 * Reverse the cards array if the length of the array is odd
	 * 
	 * @param cards
	 *            The array of cards may be reversed
	 */
	private void reverseCards(Card[] cards) {
		if (isOdd(cards.length)) {
			ArrayList<Card> allCardsList = new ArrayList<Card>();
			super.cardsListGen(cards, allCardsList);

			for (int i = 0; i < (cards.length - 1) / 2; i++) {
				allCardsList.add(allCardsList.get(0));
				allCardsList.remove(0);
			}

			for (int i = 0; i < cards.length; i++) {
				cards[i] = allCardsList.get(i);
			}
		}
	}

	/**
	 * Find the best hand from an array list of hands
	 * 
	 * @param hands
	 *            An array list of hands
	 * @return The best hand among hand
	 */
	private Hand findBestHand(ArrayList<Hand> hands) {
		if (hands == null) {
			return null;
		}

		Hand currentHand = hands.get(0);
		for (int i = 0; i < hands.size(); i++) {
			if (hands.get(i).compareTo(currentHand) < 0) {
				currentHand = hands.get(i);
			}
		}

		return currentHand;
	}

	@Override
	public Hand getBestHand(Card[] allCards) {
		return super.getBestHand(allCards);
	}

}
