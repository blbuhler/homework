package hw3;

import api.Card;
import api.Hand;

/**
 * Evaluator for a hand in which the rank of each card is a prime number. The
 * number of cards required is equal to the hand size.
 * 
 * The name of this evaluator is "All Primes".
 */
// Note: You must edit this declaration to extend AbstractEvaluator
// or to extend some other class that extends AbstractEvaluator
public class AllPrimesEvaluator extends AbstractEvaluator {
	/**
	 * Constructs the evaluator.
	 * 
	 * @param ranking
	 *            ranking of this hand
	 * @param handSize
	 *            number of cards in a hand
	 */
	public AllPrimesEvaluator(int ranking, int handSize) {
		super();
		super.ranking = ranking;
		super.handSize = handSize;
		super.numMainCards = handSize;
		super.name = "All Primes";
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public int getRanking() {
		return super.getRanking();
	}

	@Override
	public int cardsRequired() {
		return super.cardsRequired();
	}

	@Override
	public int handSize() {
		return super.handSize();
	}

	@Override
	public boolean canSubsetSatisfy(Card[] allCards) {
		return super.canSubsetSatisfy(allCards);
	}

	@Override
	public boolean canSatisfy(Card[] mainCards) {

		if (mainCards.length != cardsRequired()) {
			return false;
		} else {
			int[] ranks = new int[mainCards.length];
			for (int i = 0; i < mainCards.length; i++) {
				ranks[i] = mainCards[i].getRank();
			}

			return isArrayPrime(ranks);
		}
	}

	@Override
	public Hand createHand(Card[] allCards, int[] subset) {
		return super.createHand(allCards, subset);
	}

	@Override
	public Hand getBestHand(Card[] allCards) {
		return super.getBestHand(allCards);
	}

	private boolean isArrayPrime(int[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			if (!isPrime(numbers[i])) {
				return false;
			}
		}
		return true;
	}

	private boolean isPrime(int n) {

		// 1 is not prime
		if (n == 1) {
			return false;
		}

		// When the number is greater than 2
		for (int i = 2; i < n; i++) {
			if (n % i == 0)
				return false;
		}

		// When the number is 2
		return true;
	}

}
