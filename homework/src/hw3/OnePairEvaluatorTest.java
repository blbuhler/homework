package hw3;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import api.Card;
import api.Hand;
import api.IEvaluator;

/**
 * This will test the OnePairEvaluator using the examples from the Assigment3 specification.
 * @Author Jason Nicholson
 * @Version 2014-April-20 17:33:45
 */
public class OnePairEvaluatorTest {
    
    private int evalRank;
    private int evalHandSize;
    private int cardsRequired;
    private IEvaluator eval;
    
    // Card sets
    private Card[] cards1;
    private Card[] cards2;
    private Card[] cards3;
    private Card[] cards4;
    
    
    
    
    @Before
    public final void setup() {
        evalRank =3;
        evalHandSize = 4;
        cardsRequired = 2;
        eval = new OnePairEvaluator(evalRank, evalHandSize);
        
        cards1 = Card.createArray("2c, 2d");
        cards2 = Card.createArray("Kc, Qd");
        cards3 = Card.createArray("3h, 2c, 2d");
        cards4 = Card.createArray("6s, Jh, Ah, 10h, 6h, Js, 6c, Kh, Qh");
        Arrays.sort(cards4); // now [Ah, Kh, Qh, Js, Jh, 10h, 6s, 6h, 6c] 
    }

    @Test
    public final void testCanSatisfy1() {
        assertTrue(eval.canSatisfy(cards1));
    }
    
    @Test
    public final void testCanSatisfy2() {
        assertFalse(eval.canSatisfy(cards2));
    }
    
    @Test
    public final void testCanSatisfy3() {
        assertFalse(eval.canSatisfy(cards3));
    }
    

    @Test
    public final void testGetName() {
        assertTrue(eval.getName().equals("One Pair"));
    }

    @Test
    public final void testGetRanking() {
        assertEquals(evalRank,eval.getRanking());
    }

    @Test
    public final void testHandSize() {
        assertEquals(evalHandSize, eval.handSize());
    }

    @Test
    public final void testCardsRequired() {
        assertEquals(cardsRequired, eval.cardsRequired());
    }

    @Test
    public final void testCanSubsetSatisfy1() {
        assertTrue(eval.canSubsetSatisfy(cards3));
    }
    
    @Test
    public final void testCanSubsetSatisfy2() {
        assertTrue(eval.canSubsetSatisfy(cards4));
    }    

    @Test
    public final void testCreatehand1() {
        int[] subset1OfCards4 = {6,8};
        Hand testHand = eval.createHand(cards4, subset1OfCards4);
        Card[] mainCards = {cards4[subset1OfCards4[0]], cards4[subset1OfCards4[1]]}; //6s 6c
        Card[] sideCards = {cards4[0], cards4[1]}; // Ah Kh
        
        Hand correctHand = new Hand(mainCards, sideCards, eval);
        assertEquals(0, correctHand.compareTo(testHand));
    }
    
    @Test
    public final void testCreatehand2() {
        int[] subset2OfCards4 = {0,3};
        Hand testHand = eval.createHand(cards4, subset2OfCards4);
        
        Hand correctHand = null;
        assertEquals(correctHand, testHand);
    } 
    
    
    @Test
    public final void testGetBestHand(){
        Hand testHand = eval.getBestHand(cards4);
        Card[] mainCards = {cards4[3], cards4[4]}; // Js Jh
        Card[] sideCards = {cards4[0], cards4[1]}; // Ah Kh
        Hand correctHand = new Hand(mainCards, sideCards, eval);
        assertEquals(0,correctHand.compareTo(testHand));
    }
}