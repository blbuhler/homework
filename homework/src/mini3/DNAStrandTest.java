package mini3;

public class DNAStrandTest {
	public static void main(String[] args){
		DNAStrand strand1 = new DNAStrand("TCAT");
		DNAStrand strand2 = new DNAStrand("AGAGCAT");
		System.out.println(strand1.findMatchesWithLeftShift(strand2, 3));
		System.out.println(strand2.findMatchesWithLeftShift(strand1, 3));
		System.out.println(strand2);
	}
}
