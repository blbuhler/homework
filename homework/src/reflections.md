Refactoring the whole when you have found they are all WRONG


I should have submitted earlier if someone had not notified me that the parameter subset passed to createHand are indices of the array. Because I have been mistakenly thought indices are like ranks of the cards. Sorry for my knowledge of English and little experience of poker game.
 
Rather than createHand by indices, my createHand method use the given ranks.  And to implement that I created many helper methods, like a method to find a given card's indices, or a method using recursion to find and remove given cards from a given array, but in createdHand by indices the known indices are enough. Writing wrong methods has costed me great time.The getBestHand can also be implemented using the createHandByRank method. Strange?
 
However, I started refactoring them by commenting out all methods that are related to the createHandByRank method and rewriting whole methods this afternoon. It takes me about 5 hours to rewrite and debug them, less than the time I have spent on the wrong methods. Not so bad.
 
The point is if you have found your code is wrong, don't keep them and delete them as much as possible. Because the redundant code does not make any sense.  Keeping a habit of writing clean code and breaking them into functional parts will save a lot of time if you plan to refactor. 
 
I hope it is our last homework. 
 
======
 
Anyway, I have not deleted them as a whole and just use a git branch to rewrite methods, because for me those methods are cool.
