package hw2;

/**
 * Class representing a game of hangman. This class encapsulates all aspects of
 * the game state, including the secret word or phrase, the letters guessed so
 * far, and the number of wrong guesses. A hangman game can be constructed with
 * a specified maximum number of wrong guesses which defaults to
 * DEFAULT_MAX_WRONG_GUESSES. The secret word or phrase is represented as an
 * array of HideableChar and may contain whitespace and arbitrary punctuation
 * characters. Clients of this code decide how to represent the "hidden"
 * characters to the user. Non-alphabetic characters are normally not hidden.
 * 
 * @author Siyu Lin
 * 
 */
public class Game {

	/**
	 * Default value for the maximum number of wrong guesses.
	 */
	public static final int DEFAULT_MAX_WRONG_GUESSES = 7;

	/**
	 * Phrase to be guessed
	 */
	private String secretPhrase = "";

	/**
	 * Phase guessed by user
	 */
	private String guessedLetters = "";

	/**
	 * Maximum Guesses for a game
	 */
	private int maxGuess;

	/**
	 * Wrong guesses made by users
	 */
	private int wrongGuesses = 0;

	/**
	 * A HideableChar array of secret phrases with hidden status
	 */
	private HideableChar[] chars;

	/**
	 * Constructs a hangman game using the given word as the secret word and the
	 * default maximum number of wrong guesses.
	 * 
	 * @param word
	 *            the secret word
	 */
	public Game(String word) {
		secretPhrase = word;
		maxGuess = DEFAULT_MAX_WRONG_GUESSES;

		secretPhrase = secretPhrase;

		// Construct the HideableChar array
		chars = new HideableChar[secretPhrase.length()];
		for (int i = 0; i < secretPhrase.length(); i++)
		{
			chars[i] = new HideableChar(secretPhrase.charAt(i));
		}

	}

	/**
	 * Constructs a hangman game using the given word as the secret word and the
	 * given value as the maximum number of wrong guesses.
	 * 
	 * @param word
	 *            the secret word
	 * @param maxGuesses
	 */
	public Game(String word, int maxGuesses) {
		secretPhrase = word;
		maxGuess = maxGuesses;

		secretPhrase = secretPhrase;

		// Construct the HideableChar array
		chars = new HideableChar[secretPhrase.length()];
		for (int i = 0; i < secretPhrase.length(); i++)
		{
			chars[i] = new HideableChar(secretPhrase.charAt(i));
		}
	}

	// Methods

	/**
	 * getDisplayedWord() Returns a sequence of HideableChar representing the
	 * secret word or phrase. Letters that have not been guessed yet are hidden.
	 * Non-alphabetic characters (according to the method
	 * Character.isAlphabetic) are never hidden; the HideableChar constructor
	 * will normally help enforce this condition.
	 * 
	 * @return displayed form of the secret word
	 */
	public HideableChar[] getDisplayedWord() {
		return chars;
	}

	/**
	 * Determines whether this game is over.
	 * 
	 * @return true if the player has won or has run out of guesses, false
	 *         otherwise
	 */
	public boolean gameOver() {
		boolean isOver = won() || (wrongGuesses >= maxGuess);
		return isOver;
	}

	/**
	 * Returns a string containing all the letters guessed so far by the player,
	 * without duplicates.
	 * 
	 * @return letters guessed so far by the player
	 */
	public String lettersGuessed() {
		return guessedLetters;
	}

	/**
	 * Invoked by the player to guess a letter.
	 * 
	 * @param ch
	 *            the letter to check
	 * @return true if the letter has not been guessed already and occurs in the
	 *         secret word, false otherwise
	 */
	public boolean guessLetter(char ch) {
		
		//Convert to upper case
//		char guessedCh = Character.toUpperCase(ch);
		char guessedCh = ch;
		
		boolean isGuessed = (guessedLetters.indexOf(guessedCh) >= 0);
		boolean inSecretPhrase = (secretPhrase.indexOf(guessedCh) >= 0);
		boolean isLetter = Character.isAlphabetic(guessedCh);
		boolean isRight = !isGuessed && inSecretPhrase && isLetter;

		// If the letter has not previously been guessed and does occur in the
		// secret word, changes all occurrences of the letter in the secret word
		// to "not hidden" and returns true.
		addGuessedLetter(guessedCh);
		if (!isRight && !gameOver())
		{
			wrongGuesses++;
		}
		else
		{
			unHideIfRightGuess(guessedCh);
		}
		return isRight;
	}

	/**
	 * Unhide the character if it matches the guess
	 * 
	 * @param ch
	 *            character guessed
	 */
	private void unHideIfRightGuess(char ch) {
		// Change the hidden status of the right character
		for (int i = 0; i < secretPhrase.length(); i++)
		{

			// Display the guessed and correct character
			if (chars[i].matches(ch))
			{
				chars[i].unHide();
			}
		}
	}

	/**
	 * Append a letter to the guessed letters if the letter has not been guessed
	 * before
	 * 
	 * @param ch
	 *            character guessed
	 */
	private void addGuessedLetter(char ch) {
		boolean isGuessed = (guessedLetters.indexOf(ch) >= 0);
		if (isGuessed || gameOver())
		{
			// Do nothing because duplicates are not allowed
		}
		else
		{
			guessedLetters = guessedLetters + ch;
		}

	}

	/**
	 * Returns the number of wrong guesses made so far by the player.
	 * 
	 * @return number of wrong guesses
	 */
	public int numWrongGuesses() {
		return wrongGuesses;
	}

	/**
	 * Determines whether the player has guessed all the letters in the secret
	 * word.
	 * 
	 * @return true if the player has won, false otherwise
	 */
	public boolean won() {
		
		//Return false when wrong guesses has reached the max guesses
		if (wrongGuesses >= maxGuess){
			return false;
		}
		
		int i = 0;

		// Break the loop if the last character is checked or one character is
		// hidden
		while ((i < secretPhrase.length()) && !chars[i].isHidden())
		{
			i++;
		}

		// Check if all the characters are displayed
		boolean allDisplayed = (i == secretPhrase.length());
		return allDisplayed;
	}

	/**
	 * Returns the maximum number of wrong guesses for this game.
	 * 
	 * @return maximum number of wrong guesses
	 */
	public int getMaxGuesses() {
		return maxGuess;
	}

	/**
	 * Returns the complete secret word or phrase as a string.
	 * 
	 * @return the secret word or phrase
	 */
	public String getSecretWord() {
		return secretPhrase;
	}

}
