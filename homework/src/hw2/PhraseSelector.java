package hw2;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Generator for secret words or phrases for word-guessing games. A
 * PhraseSelector chooses a line randomly from a file specified in the
 * constructor.
 * 
 * @author Siyu Lin
 * 
 */
public class PhraseSelector {
	
	/**
	 * Name of file to be read
	 */
	private String fileName;
	
	/**
	 * Lines of phrases from the file
	 */
	private ArrayList<String> words = new ArrayList<String>();

	/**
	 * Constructs a PhraseSelector that will select words from the given file.
	 * This constructor may throw a FileNotFoundException if the file cannot be
	 * opened.
	 * 
	 * @param givenFileName
	 *            the name of the file
	 * @throws java.io.FileNotFoundException
	 */
	public PhraseSelector(String givenFileName) throws java.io.FileNotFoundException {
		fileName = givenFileName;

		// Open the file and initialize the array list
		File file = new File(fileName);
		Scanner scanner = new Scanner(file);
		while (scanner.hasNextLine())
		{
			// Get the next line and remove the spaces
			String line = scanner.nextLine();
			line = line.trim();
			words.add(line);
		}
		scanner.close();
	}

	/**
	 * Returns a word or phrase selected at random from this PhraseSelector's
	 * file, using the given source of randomness.
	 * 
	 * @param rand
	 * @return a randomly selected line of the file
	 */
	public String selectWord(Random rand) throws java.io.FileNotFoundException {
		// Randomly select an element from the array list
		int index = rand.nextInt(words.size());
		String word = words.get(index);
		return word;
	}

}
