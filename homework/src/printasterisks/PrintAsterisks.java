package printasterisks;

public class PrintAsterisks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printAsterisks(3);
		
	}
	public static void reverseDiagonalStars(int n){
		for(int rows = 0; rows < n; rows ++){
			for(int columns = 0; columns < n-rows-1; columns++){
				System.out.print(" ");
			}
			System.out.print("*");
			System.out.println();
		}
	}
	public static void printAsterisks(int n){
		for (int i = 1; i < n; i = i + 2)
		{
			for (int j = (n-i)/2; j > 0; j --)
			{
				System.out.print(" ");
			}
			
			for(int k = i; k > 0; k --)
			{
				System.out.print("*");
			}
			System.out.println();
		}
		for (int i = n; i > 0; i = i - 2)
		{
			for (int j = (n-i)/2; j > 0; j --)
			{
				System.out.print(" ");
			}
			
			for(int k = i; k > 0; k --)
			{
				System.out.print("*");
			}
			System.out.println();
		}
	}
	
	
}
