package hw1;

/**
 * Model of a coin-operated ParkingMeter is constructed so that a specified
 * number of minutes is associated with each of three coin types, nickels,
 * dimes, or quarters.
 * 
 * @author Siyu Lin
 */
public class ParkingMeter {
	// Instance Variables
	/**
	 * The remaining coin values, by default in cents
	 */
	private int coinRemainingValues;
	/**
	 * The remaining coin numbers
	 */
	private int coinRemainingNumbers;
	/**
	 * The remaining time by default in minutes
	 */
	private int timeRemaining;
	/**
	 * The max number of coin that can be inserted
	 */
	private int coinMax;
	/**
	 * The max time by default in minutes
	 */
	private int timeMax;
	/**
	 * Added time for one nickel inserted, by default in minutes
	 */
	private int minutesForNickel;
	/**
	 * Added time for one dime inserted, by default in minutes
	 */
	private int minutesForDime;
	/**
	 * Added time for one quarter inserted, by default in minutes
	 */
	private int minutesForQuarter;
	/**
	 * Value of nickel coin in cents.
	 */
	public static final int NICKEL_VALUE = 5;

	/**
	 * Value of dime coin in cents.
	 */
	public static final int DIME_VALUE = 10;

	/**
	 * Value of quarter coin in cents.
	 */
	public static final int QUARTER_VALUE = 25;

	/**
	 * Constructors ParkingTime by default is initially expired and contains no
	 * coins
	 * 
	 * @param givenMinutesForNickel
	 *            Added time for one nickel inserted, by default in minutes,
	 * @param givenMinutesForDime
	 *            Added time for one dime inserted, by default in minutes,
	 * @param givenMinutesForQuarter
	 *            Added time for one quarter inserted, by default in minutes,
	 * @param givenMaxTime
	 *            The max time by default in minutes
	 * @param givenMaxCoins
	 *            The max number of coin that can be inserted
	 */
	public ParkingMeter(int givenMinutesForNickel, int givenMinutesForDime,
			int givenMinutesForQuarter, int givenMaxTime, int givenMaxCoins) {
		timeRemaining = 0;
		coinMax = givenMaxCoins;
		timeMax = givenMaxTime;
		minutesForNickel = givenMinutesForNickel;
		minutesForDime = givenMinutesForDime;
		minutesForQuarter = givenMinutesForQuarter;
		// Check the validity of the numbers
		if (minutesForNickel > minutesForDime || minutesForDime > minutesForQuarter)
		{
			//  All initialized to 0 if not valid
			minutesForNickel = 0;
			minutesForDime = 0;
			minutesForQuarter = 0;
			coinMax = 0;
			timeMax = 0;
		}
		if (!((minutesForNickel > 0 && minutesForDime > 0 && minutesForQuarter > 0 && coinMax > 0 && timeMax > 0)))
		{
			// All initialized to 0 if not valid
			minutesForNickel = 0;
			minutesForDime = 0;
			minutesForQuarter = 0;
			coinMax = 0;
			timeMax = 0;
		}

	}

	// Methods

	/**
	 * simulateTimeSimulates the passage of time for the given number of
	 * minutes. Does nothing if the given value is negative 
	 * 
	 * @param minutes
	 *            The passage of time, in minutes by default
	 */
	public void simulateTime(int minutes) {
		// Check the validity of the minutes
		// check if the minutes is negative
		int elapsedMinutes = minutes;
		if (minutes > 0)
		{
			// When the elapsedMinutes has reached the timeRemaining, the
			// timeRemaining should be 0
			timeRemaining = Math.max(timeRemaining - elapsedMinutes, 0);
		}
		else
		{
			// Does nothing if negative
		}

	}

	/**
	 * emptyCoins reduces the coin to zero, without modifying the time
	 */
	public void emptyCoins() {
		coinRemainingNumbers = 0;
		coinRemainingValues = 0;
	}

	/**
	 * getCents returns the total amount of money, in cents, contained in this
	 * meter
	 * 
	 * @return the total amount of money
	 */
	public int getCents() {
		return coinRemainingValues;
	}

	/**
	 * getDollarString returns a String representing the total amount of money,
	 * in dollars
	 * 
	 * @return the total amount of money
	 */
	public String getDollarString() {
		// By default the coinRemainingValues is represented in cents, so it times 0.01
		String result = String.format("%01.2f", coinRemainingValues * 0.01);
		return result;
	}

	/**
	 * getMinutesRemaining returns the amount of time, in minutes, remaining on
	 * this meter
	 * 
	 * @return the amount of time
	 */
	public int getMinutesRemaining() {
		return timeRemaining;
	}

	/**
	 * getHourMinuteString() returns a String representing the time remaining on
	 * the meter in the form "hh:mm"'
	 * 
	 * @return the time remaining on the meter in the form "hh:mm"'
	 */
	public String getHourMinuteString() {
		// get the remaining hours by dividing the remaining minutes
		int h = timeRemaining / 60;
		// get the remaining minutes by deducting the hours
		int m = timeRemaining - h * 60;
		String result = String.format("%02d:%02d", h, m);
		return result;
	}

	/**
	 * getTotalCoins() returns the number of coins currently in this meter
	 * 
	 * @return the number of coins
	 */
	public int getTotalCoins() {
		return coinRemainingNumbers;
	}

	/**
	 * addingCoins will increase the coinRemainingNumber for a given number of
	 * coins inserted until the coinMax
	 * 
	 * @param howMany
	 *            The number of coins inserted
	 */
	private void addingCoins(int howMany) {
		int addedCoins = howMany;
		// check the validity of the addedCoins
		if (addedCoins < 0)
		{
			addedCoins = 0;
		}
		else
		{
			// The coinRemainingNumbers can never goes beyond the coinMax
			coinRemainingNumbers = Math.min(coinRemainingNumbers + addedCoins, coinMax);
		}
	}

	/**
	 * insertDimes(int howMany) inserts a given number of dimes into this meter,
	 * increasing the coinRemainingNumerbers, coinRemainingValues, timeRemaining
	 * until their limits
	 * 
	 * @param howMany
	 *            The number of coins inserted
	 */
	public void insertDimes(int howMany) {
		// Number of the dimes added
		int addedDimesNumber;
		addedDimesNumber = howMany;
		// The coinRemainingNumbers before adding
		int currentCoinRemainingNumbers = coinRemainingNumbers;
		// Increase the coin numbers after inserting the dimes
		this.addingCoins(addedDimesNumber);
		// Coins can only be inserted until the coinMax
		if (coinRemainingNumbers == coinMax)
		{
			addedDimesNumber = coinMax - currentCoinRemainingNumbers;
		}
		// Increase time after inserting coins
		// Check the validity of the parameter howMany
		if (addedDimesNumber > 0)
		{
			// It would be ignored if the time has exceeded the max time amount
			timeRemaining = Math.min(timeRemaining + addedDimesNumber * minutesForDime, timeMax);
			// Only when the timeRemaining has exceeded the timeMax and the
			// coinRemainingNumbers has not exceeded the coinMax,
			// the coinRemainingValues can be added
			if ((timeRemaining <= timeMax) && (coinRemainingNumbers <= coinMax))
			{
				coinRemainingValues = coinRemainingValues + addedDimesNumber * DIME_VALUE;
			}
			else
			{
				// The coinRemainingValues will not increase if timeRemaining or
				// the coinRemainingNumber has reached their limits
			}
		}
	}

	/**
	 * insertNickels(int howMany) inserts a given number of dimes into this
	 * meter, increasing the coinRemainingNumerbers, coinRemainingValues,
	 * timeRemaining until their limits
	 * 
	 * @param howMany
	 *            The number of coins inserted
	 */
	public void insertNickels(int howMany) {
		// Number of the nickels added
		int addedNickelsNumber;
		addedNickelsNumber = howMany;
		// The coinRemainingNumbers before adding
		int currentCoinRemainingNumbers = coinRemainingNumbers;
		this.addingCoins(addedNickelsNumber);
		// When the coinRemainingNumbers has reached the coinMax, the coins can
		// not be inserted any more
		if (coinRemainingNumbers == coinMax)
		{
			addedNickelsNumber = coinMax - currentCoinRemainingNumbers;
		}
		if (addedNickelsNumber > 0)
		{
			// It would be ignored if the time has exceeded the max time amount
			timeRemaining = Math.min(timeRemaining + addedNickelsNumber * minutesForNickel, timeMax);
			// Only when the timeRemaining has exceeded the timeMax and the
			// coinRemainingNumbers has not exceeded the coinMax,
			// the coinRemainingValues can be added
			if ((timeRemaining <= timeMax) && (coinRemainingNumbers <= coinMax))
			{
				coinRemainingValues = coinRemainingValues + addedNickelsNumber * NICKEL_VALUE;
			}
			else
			{
				// The coinRemainingValues will not increase if timeRemaining or
				// the coinRemainingNumber has reached their limits
			}
		}
	}

	/**
	 * insertQuarters(int howMany) inserts a given number of dimes into this
	 * meter, increasing the coinRemainingNumerbers, coinRemainingValues,
	 * timeRemaining until their limits
	 * 
	 * @param howMany
	 *            The number of coins inserted
	 */
	public void insertQuarters(int howMany) {
		// number of the quarters added
		int addedQuartersNumber;
		addedQuartersNumber = howMany;
		// The coinRemainingNumbers before adding
		int currentCoinRemainingNumbers = coinRemainingNumbers;
		this.addingCoins(addedQuartersNumber);
		// When the coinRemainingNumbers has reached the coinMax, the coins can
		// not be inserted any more
		if (coinRemainingNumbers == coinMax)
		{
			addedQuartersNumber = coinMax - currentCoinRemainingNumbers;
		}
		if (addedQuartersNumber > 0)
		{
			// /It would be ignored if the time has exceeded the max time amount
			timeRemaining = Math.min(timeRemaining + addedQuartersNumber * minutesForQuarter, timeMax);
			// Only when the timeRemaining has exceeded the timeMax and the
			// coinRemainingNumbers has not exceeded the coinMax,
			// the coinRemainingValues can be added
			if ((timeRemaining <= timeMax) && (coinRemainingNumbers <= coinMax))
			{
				coinRemainingValues = coinRemainingValues + addedQuartersNumber * QUARTER_VALUE;
			}
			else
			{
				// The coinRemainingValues will not increase if timeRemaining or
				// the coinRemainingNumber has reached their limits
			}

		}
		// does nothing if addedQuatersNumber is negative
	}

	/**
	 * isExpired() returns true if there is no time remaining, false otherwise.
	 * 
	 * @return returns true if there is no time remaining, false otherwise.
	 */
	public boolean isExpired() {
		if (timeRemaining > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * toString returns a String representation of this object in exactly the
	 * following format. A newly constructed ParkingMeter with a 120 minute time
	 * limit and a capacity of 1000 coins, toString would return the string:
	 * hw1.ParkingMeter [Time 0/120, Coins 0/1000, Value 0]
	 * 
	 * @return a String representation
	 */
	public String toString() {
		String time = "Time " + timeRemaining + "/" + timeMax;
		String number = "Coins " + coinRemainingNumbers + "/" + coinMax;
		String value = "Value " + coinRemainingValues;
		String result = "hw1.ParkingMeter [" + time + ", " + number + ", " + value + "]";
		return result;
	}

}
