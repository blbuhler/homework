package lab8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MileageCalculator {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		ArrayList<FillUp> list = new ArrayList<FillUp>();
		list = readFillUp();
		double[] results = new double[10];
		int[] currentOdo = new int[10];
		int[] previousOdo = new int[10];
		double[] gallons = new double[10];
		
		previousOdo[0] = list.get(0).getOdometer();
		
		for (int i = 1; i <= 9; i++){
			
			//Get the currentOdo, gallons
			currentOdo[i-1] = list.get(i).getOdometer();	
			gallons[i-1] = list.get(i).getGallons();
			
			//Calculate Mileage
			results[i] = calculateMileage(currentOdo[i-1], previousOdo[i-1], gallons[i-1]);
			
			//Print the result
			System.out.println("Mileage" + i + ": " + results[i]);
			
			//Get the previousOdo
			previousOdo[i] = currentOdo[i-1];
		}
		

	}

	public static ArrayList<FillUp> readFillUp() throws FileNotFoundException {
		ArrayList<FillUp> fillUp = new ArrayList<FillUp>();
		ArrayList<Integer> odo = new ArrayList<Integer>() ;
		ArrayList<Double> gal = new ArrayList<Double>() ;
		
		// open the file
		File file = new File("mileage.txt");
		Scanner scanner = new Scanner(file);
		
		int lineNumber = 0;
		
		// while there are more lines...
		while (scanner.hasNextLine())
		{
			String line = scanner.nextLine();
			Scanner temp = new Scanner(line);
			odo.add(temp.nextInt());
			gal.add(temp.nextDouble());
			FillUp currentFillUp = new FillUp(odo.get(lineNumber), gal.get(lineNumber));
			fillUp.add(currentFillUp);
			lineNumber++;
		}

		
		// close the file
		scanner.close();
		
		return fillUp;
				
	}
	
	public static double calculateMileage (int currentOdo, int previousOdo, double gallons)
	{
		int drivenMiles = currentOdo - previousOdo;
		return drivenMiles/gallons;
	}

}
