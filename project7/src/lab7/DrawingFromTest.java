package lab7;

import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import plotter.PolylinePlotter;

public class DrawingFromTest {
	public static void main(String[] args) throws FileNotFoundException {
		// Open the file
//		File file = new File("test.txt");
		File file = new File("test2.txt");
		Scanner scanner = new Scanner(file);

		PolylinePlotter plotter = new PolylinePlotter();
		// while there are more lines...
		while (scanner.hasNextLine())
		{

			// Get the next line
			String line = scanner.nextLine();

			// Remove leading and training white spaces
			line = line.trim();

			// Process the next line
			if (!line.equals(""))
			{
				if (isComment(line))
				{
					line = scanner.nextLine();
				}
				drawFromLine(line, plotter);
			}
		}

		// Close the file
		scanner.close();
	}

	private static void drawFromLine(String line, PolylinePlotter plotter) {
		// construct a temporary scanner, just to read data from this line
		Scanner temp = new Scanner(line);
		int width = 1;

		// Width
		if (temp.hasNextInt())
		{
			width = temp.nextInt();
		}
		// get the color
		String color = temp.next();

		// StartPoint
		int startPointX = temp.nextInt();
		int startPointY = temp.nextInt();
		Point startPoint = new Point(startPointX, startPointY);
		plotter.startLine(color, startPoint, width);

		// Coordinates
		int[] coordinate = new int[line.length()];
		int currentSize = 0;
		while (temp.hasNextInt())
		{
			int tempInt = temp.nextInt();
			coordinate[currentSize] = tempInt;
			currentSize++;
		}

		// Connect all the coordinates
		for (int i = 0; i < currentSize; i = i + 2)
		{
			int eachPointX = coordinate[i];
			int eachPointY = coordinate[i + 1];
			Point eachPoint = new Point(eachPointX, eachPointY);
			plotter.addPoint(eachPoint);
		}
	}
	
	private static boolean isComment(String line){
		Scanner temp = new Scanner (line);
		String leading = temp.next();
		if (leading.equals("#"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
