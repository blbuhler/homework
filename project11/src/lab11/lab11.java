package lab11;

import java.io.File;
import java.util.ArrayList;

public class lab11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(power(5));
		System.out.println(getPyramidCount(7));
		
		int[] test = {3, 4, 5, 7, 2, 3, 2}; // sum should be 20
	    int result = arrayMax(test);
	    System.out.println(result);
	    
	    File testFile = new File("../project10/src/lab10"); 
	    System.out.println(sizeOfDir(testFile));
	    ArrayList<String> javaNames = javaFilesFinder(testFile);
	    for(int i = 0; i < javaNames.size(); i++){
	    	System.out.println(javaNames.get(i));
	    }
	}

	public static int power(int p) {
		if (p == 0) {
			return 1;
		} else {
			power(p - 1);
			return power(p - 1) * 2;
		}
	}

	public static int getPyramidCount(int levels) {
		if (levels == 0) {
			return 0;
		} else {
			getPyramidCount(levels - 1);
			return getPyramidCount(levels - 1) + levels * levels;
		}
	}

	public static int arrayMax(int[] arr) {
		int result = arrayMaxVerbose(arr, 0, arr.length - 1, 0);
		return result;
	}

	private static String subArrayToString(int[] arr, int start, int end) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		if (start <= end) {
			sb.append(arr[start]);
		}
		for (int i = start + 1; i <= end; ++i) {
			sb.append(", " + arr[i]);
		}

		sb.append("]");
		return sb.toString();
	}

	private static String makeSpaces(int n) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; ++i) {
			sb.append(" ");
		}
		return sb.toString();
	}
	
	 private static int arrayMaxVerbose(int[] arr, int start, int end, int depth)
	  {
	    if (start == end)
	    {
	      return arr[start];
	    }
	    else
	    {

	      int mid = (start + end) / 2;
	      String leftString = subArrayToString(arr, start, mid);
	      
	      // recursively find max for left subarray
	      int leftMax = arrayMaxVerbose(arr, start, mid, depth);
	      
	      
	      // recursively find max for right subarray, line up output
	      int rightDepth = depth + leftString.length() + 1;
	      int rightMax = arrayMaxVerbose(arr, mid + 1, end, rightDepth);
	      
	      //Compare left and right array and the get the new array
	      if(leftMax > rightMax){
	    	  return leftMax;
	      }
	      else{
	    	  return rightMax;
	      }
	      
	    }
	  }
	 
	 public static long sizeOfDir(File file){
		 long result = 0;
		 if(file.isFile()){
			 result = result + file.length();
		 }
		 else{
			 //If it is not file
			 //Find 
			 File[] files = file.listFiles();
			 for(int i = 0; i < files.length; i++){
				 result = result + sizeOfDir(files[i]);				 
			 }
		 }
		 return result;
	 }
	 
	 public static void javaFilesFinderVerbose(File file, ArrayList<String> names){
		 if(file.isFile() && isJavaFile(file.getName())){
			  names.add(file.getName());
		 }
		 else if(file.isDirectory()) {
			 File[] files = file.listFiles();
			 for(int i = 0; i < files.length; i++){
				 javaFilesFinderVerbose(files[i], names);
			 }
		 }
		 else{
			 return;
		 }
	 }
	 
	 private static boolean isJavaFile(String name){
		 if(name.contains(".java")){
			 return true;
		 }
		 else{
			 return false;
		 }
	 }
	 
	 public static ArrayList<String> javaFilesFinder(File file){
		 ArrayList<String> results = new ArrayList<String>();
		 javaFilesFinderVerbose(file, results);
		 return results;
	 }

}
