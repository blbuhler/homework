package lab2;

public class AtomTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Generate an atom called uranium
		Atom uranium;
		uranium = new Atom(92, 146, 92);
		// Print the mass of the uranium
		int uraniumMass = uranium.getAtomicMass();
		System.out.println(uraniumMass);
		// Print the charge of the uranium
		int uraniumAtomaticCharge = uranium.getAtomicCharge();
		System.out.println(uraniumAtomaticCharge);
		// Print the mass and the charge of the decayed Uranium
		uranium.decay();
		int uraniumMassDecayed = uranium.getAtomicMass();
		System.out.println(uraniumMassDecayed);
		int uraniumAtomaticChargeDecayed = uranium.getAtomicCharge();
		System.out.println(uraniumAtomaticChargeDecayed);
	}

}
