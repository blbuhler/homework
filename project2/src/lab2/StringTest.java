package lab2;

public class StringTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String message;
		message = "Hello, world!";
		System.out.println(message);
		// Print the length of the message
		int theLength = message.length();
		System.out.println(theLength);
		// Print the message's first letter 
		System.out.println(message.charAt(0));
		// Print the message's second letter 
		System.out.println(message.charAt(1));
		// Print the message in uppercase
		String messageCap = message.toUpperCase();
		System.out.println(messageCap);
		// Print the strings "world!"
		String messageSub = message.substring(7);
		System.out.println(messageSub);
		// Print the message "Hello,world?"
		String messageReplacement = message.replace("!", "?");
		System.out.println(messageReplacement);
		
	}

}
