package lab4;

import java.util.Scanner;
import lab2.Atom;

public class UsingDelimiter {
	public static void main(String[] args)
	{
		Scanner parser = new Scanner("Uranium,U,92,92,146");
		parser.useDelimiter(",");
		
		//Skip the first two fields
		parser.next();
		parser.next();
		int protonCount = parser.nextInt();
		int electronCount = parser.nextInt();
		int neutronCount = parser.nextInt();
		
		Atom u228 = new Atom (protonCount, neutronCount, electronCount);
		
	}
}
