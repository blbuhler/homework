package chapter1;
/*
ID: siyul1
LANG: JAVA
TASK: PROG: gift1
*/
import java.io.*;
import java.util.*;

public class GreedyGiftGivers {
	public static void main(String[] args) throws IOException {
		BufferedReader f = new BufferedReader(new FileReader("gift1.in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				"gift1.out")));
		//NP
		StringTokenizer line1 = new StringTokenizer(f.readLine());
		int np = Integer.parseInt(line1.nextToken()); 
		ArrayList<String> names = new ArrayList<String>();
		int lineNum = 1;
		while (lineNum <= np ) {
			StringTokenizer name = new StringTokenizer(f.readLine());
			names.add(name.nextToken());
			lineNum ++;
		}
		
		ArrayList<Integer> balance = new ArrayList<Integer>();
		
		//Giver Name
		int end = lineNum;
		String giver = "";
		int[] amounts = new int[2];
		ArrayList<String> rNames = new ArrayList<String>();
		while (lineNum <= end ) {
			if(lineNum == end) {
				StringTokenizer name = new StringTokenizer(f.readLine());
				giver = name.nextToken();
				StringTokenizer allocAmount = new StringTokenizer(f.readLine());
				int total = Integer.parseInt(allocAmount.nextToken());
				int nRec = Integer.parseInt(allocAmount.nextToken());
				amounts[0] = total;
				amounts[1] = nRec;
				readGroupGiver(giver, amounts, names, balance);
				end += nRec + 1;
				lineNum ++;
			}
			else{
				StringTokenizer name = new StringTokenizer(f.readLine());
				String reciever = name.nextToken();
				rNames.add(reciever);
			}
			lineNum ++;
		}
		//Recipient Names
		//ReadGroupGiver and readGroups Receivers
		
		

		

		f.close();
		out.close();
		System.exit(0); // don't omit this!
	}
	
	public static void readGroupGiver(String name, int[] amounts, ArrayList<String> names, ArrayList<Integer> balance){
		if(amounts[1] == 0){
			return;
		}
		//Search name who is the giver in names
		int nameIndex = searchName(name, names);
		//Get giver's amount
		int change = amounts[0] % amounts[1];
		int previous = balance.get(nameIndex);
		balance.set(nameIndex, previous-change);
	}

	private static int searchName(String name, ArrayList<String> names) {
		
		int nameIndex = 0;
		for(int i = 0; i < names.size(); i ++){
			if(names.get(i).equals(name)){
				nameIndex = i;
			}
		}
		return nameIndex;
	}
	
	public static void readGroupReceiver(ArrayList<String> rNames, int[] amounts, ArrayList<String> names, ArrayList<Integer> balance){
		//Search the name and get index
		//Increase the balance
		if(amounts[1] == 0){
			return;
		}
		int change = amounts[0] / amounts[1];
		for(String name:rNames){
			int nameIndex = searchName(name, names);
			int previous = balance.get(nameIndex);
			balance.set(nameIndex, previous+change);
		}
	}
	
}
	
	