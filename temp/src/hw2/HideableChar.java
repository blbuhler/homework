package hw2;

/**
 * Class representing a hidden character for a word-guessing game. Each instance
 * encapsulates one character, which may have a status of "hidden" or
 * "not hidden". When in the hidden state, getDisplayedChar returns null; if not
 * hidden, getDisplayedChar returns the encapsulated character as a
 * one-character string.
 * 
 * @author Siyu Lin
 * 
 */
public class HideableChar {

	// Instance Variables
	/**
	 * Character with hidden status
	 */
	private char character;

	/**
	 * Hidden status of the character
	 */
	private boolean hidden;

	// Constructor

	/**
	 * Constructs a HideableChar with the given character as data.
	 * 
	 * @param ch
	 */
	public HideableChar(char ch) {
		character = ch;
		hidden = Character.isAlphabetic(character);
	}

	// Methods

	/**
	 * Determines whether this object is currently in the hidden state.
	 * 
	 * @return true if this object is hidden, false otherwise
	 */
	public boolean isHidden() {
		return hidden;
	}

	/**
	 * Returns null if this object is in the hidden state, otherwise returns a
	 * one-character string consisting of the character stored in this object.
	 * 
	 * @return string consisting of this object's character, or null if hidden
	 */
	public String getDisplayedChar() {
		if (hidden)
		{
			return null;
		}
		else
		{
			return this.getHiddenChar();
		}
	}

	/**
	 * Returns a one-character string consisting of the character stored in this
	 * object. (whether hidden or not).
	 * 
	 * @return string consisting of this object's character
	 */
	public String getHiddenChar() {
		return "" + character;
	}

	/**
	 * Sets this object's state to hidden.
	 */
	public void hide() {
		hidden = true;
	}

	/**
	 * Sets this object's state to not hidden.
	 */
	public void unHide() {
		hidden = false;
	}

	/**
	 * Determines whether the given character is equal to the character stored
	 * in this object.
	 * 
	 * @param ch
	 *            given character to check
	 * @return true if the given character is equal to this object's data
	 */
	public boolean matches(char ch) {
		return character == ch;
	}

}
