package exam2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ContactDirectory {
	private String fileName;

	public ContactDirectory(String givenFileName) {
		fileName = givenFileName;
	}

	public void addContact(Contact c) throws FileNotFoundException {
		File file = new File(fileName);
		PrintWriter out = new PrintWriter(file);
		out.println(c.getName() + ", " + c.getPhoneNumber());
		out.close();
	}

	public String lookUp(String name) throws FileNotFoundException {
		File file = new File(fileName);
		String phone = "";
		Scanner scanner = new Scanner(file);

		while (scanner.hasNextLine()) {
			// Get the next line and remove the spaces
			String line = scanner.nextLine();
			line = line.trim();

			Scanner lineScanner = new Scanner(line);
			// Process the current line
			lineScanner.useDelimiter(",");
			if (name.equals(lineScanner.next())) {
				phone = lineScanner.next();
			} else {
				// do nothing
			}
		}
		scanner.close();
		return phone;
	}
}
