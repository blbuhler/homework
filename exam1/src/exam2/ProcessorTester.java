package exam2;

public class ProcessorTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[] testArray = {1.2, 2,3, 3.4};
		Processor maxFinder = new MaxProcessor();
		double max = ArrayProcessor.runProcessor(testArray, maxFinder);
		System.out.println(max);
		
		Processor rangeCounter = new RangeCounter(0, 3);
		System.out.println(ArrayProcessor.runProcessor(testArray, rangeCounter));
	}

}
