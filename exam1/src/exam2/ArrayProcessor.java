package exam2;

public class ArrayProcessor {
	// Processes values in an array using the given Processor
	public static double runProcessor(double[] arr, Processor p) {
		for (int i = 0; i < arr.length; ++i) {
			p.process(arr[i]);
		}
		return p.getResult();
	}

}
