package exam2;

public class MaxProcessor implements Processor {
	private double max;
	public double getResult() {
		return max;
	}

	public void process(double valToProcess) {
		if (valToProcess > max){
			max = valToProcess;
		}
	}
	
	public MaxProcessor(){
		max = Double.NEGATIVE_INFINITY;
	}

}
