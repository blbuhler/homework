package exam1;

public class Loan {
	private double balance;
	private double monthlyInterestRate;
	private final int MONTHS = 12;
	public Loan(double givenBalance, double givenAnnualInterestRate){
		balance = givenBalance;
		monthlyInterestRate = givenAnnualInterestRate/MONTHS;
	}
	public void makePayment(double payment){
		if (balance > 0)
		{
			balance = balance + balance * monthlyInterestRate - payment;
		}
		else
		{
			
		}
	}
	public double getBalance(){
		return balance;
	}
	public double getPayofAmount(){
		if (balance > 0)
		{
			return balance + balance * monthlyInterestRate;
		}
		else
		{
			return balance;
		}
	}
}
