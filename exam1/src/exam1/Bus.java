package exam1;

public class Bus {
	private int currentPassengers;
	private int maximumPassengers;
	private int stopsCount;
	public Bus() {
		currentPassengers = 0 ;
		maximumPassengers = 0;
		stopsCount = 0;
	}
	public int passengerCount(){
		return currentPassengers;
	}
	public void busStop(int passengersOn, int passengersOff){
		currentPassengers = currentPassengers + passengersOn - passengersOff;
		maximumPassengers = Math.max(maximumPassengers, currentPassengers);
		stopsCount ++;
	}
	public int maximumPassengerCount(){
		return maximumPassengers;
	}
	public int numberOfStops(){
		return stopsCount;
	}
}
