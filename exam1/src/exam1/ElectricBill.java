package exam1;

public class ElectricBill {
	 public static final double SUMMER = 0.0866; 
	 public static final double WINTER1 = 0.0798; 
	 public static final int CUTOFF1 = 400; 
	 public static final double WINTER2 = 0.0669; 
	 public static final int CUTOFF2 = 600; 
	 public static final double WINTER3 = 0.0634; 
	 public static final double ECA = 0.0151; 
	 public static final double SERVICE = 5.25; 
	 private boolean isSummer;
	 private double totalCost;
	 private int kwhUsage;
	 public ElectricBill(int givenKwh, boolean givenIsSummer){
		 kwhUsage = givenKwh;
		 isSummer = givenIsSummer;
	 }
	 public double computeTotal(){
		 double firstCut = 0;
		 double secondCut = 0;
		 double thirdCut = 0;
		 if (isSummer)
		 {
			 totalCost = kwhUsage * SUMMER;
		 }
		 else
		 {
			 firstCut = Math.min(kwhUsage, CUTOFF1) * WINTER1;
			 if (kwhUsage - CUTOFF1 > 0)
			 {
				 secondCut = Math.min(kwhUsage - CUTOFF1, CUTOFF2) * WINTER2;
			 }
			 thirdCut = Math.max(kwhUsage - CUTOFF1-CUTOFF2, 0) * WINTER3;
			 totalCost = firstCut + secondCut + thirdCut;
		 }
		 return totalCost + ECA * kwhUsage + SERVICE;
	 }
}
