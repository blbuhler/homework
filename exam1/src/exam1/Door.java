package exam1;


public class Door 
{ 
	private boolean isLocked; //ADDED
	private boolean isOpen; //ADDED
	
	public Door (boolean givenIsLocked, boolean givenIsOpen) //ADDED
	{
		isLocked = givenIsLocked; //ADDED
		isOpen = givenIsOpen;//ADDED
	}
	// Ensures the door is locked (whether open or not) 
	public void lockDoor() 
	{ 
		//boolean isLocked = true; //DELETED
		isLocked = true;//ADDED
	}	 

	// Ensures the door is unlocked (whether open or not) 
	public void unlockDoor() 
	{ 
		isLocked = false; 
	} 

	// Ensures the door is closed (it can be closed whether locked or not) 
	public void closeDoor() 
	{ 
		//boolean isOpen = false; //DELETED
		isOpen = false;//ADDED
	} 

	// Opens the door, if it is not locked. 
	public void openDoor() 
	{ 
		if (!isLocked) 
		{ 
			isOpen = true; 
		} 
	} 
	
	public boolean isOpen() 
	{ 
		return isOpen; 
	} 
} 


