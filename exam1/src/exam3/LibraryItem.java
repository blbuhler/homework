package exam3;

public abstract class LibraryItem implements Item {
	private String title;
	private int checkOutPeriod;

	protected LibraryItem(String title, int checkOutPeriod) {
		this.title = title;
		this.checkOutPeriod = checkOutPeriod;
	}

	public String getTitle() {
		return title;
	}

	public int getCheckOutPeriod() {
		return checkOutPeriod;
	}
}