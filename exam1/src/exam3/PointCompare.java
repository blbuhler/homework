package exam3;

import java.util.Arrays;

public class PointCompare {
	public static void main(String[] arg){
		Point a = new Point(1, 1);
		Point b = new Point(-1, 1);
		System.out.println(a.compareTo(b));
		Point c = new Point(0, 0);
		System.out.println(a.compareTo(c));
		Point[] points = {a, b, c};
		System.out.println(Arrays.toString(points));
		Arrays.sort(points);
		System.out.println(Arrays.toString(points));
	}
}
