package edu.iastate.cs228.hw1;

/**
 * The class MilitaryTransport include common code for all MilitaryTransport
 * types
 * 
 * @author Siyu Lin
 * 
 */
public abstract class MilitaryTransport implements
		Comparable<MilitaryTransport> {

	/**
	 * The name of the MilitaryTransport
	 */
	private String name;

	/**
	 * The defense of the MilitaryTransport
	 */
	private int defense;

	/**
	 * Constructor for a MilitaryTransport
	 * 
	 * @param name
	 *            The name for the MilitaryTransport without duplication
	 * @param defense
	 *            The defense for the MilitaryTransport, which is a positive
	 *            integer
	 */
	public MilitaryTransport(String name, int defense) {
		this.name = name;
		this.defense = defense;
	}

	/**
	 * Compare two MilitaryTransport objects by defense
	 */
	@Override
	public int compareTo(MilitaryTransport other) {
		if (this.defense > other.defense) {
			return 1;
		} else if (this.defense < other.defense) {
			return -1;
		} else
			return 0;
	}

	/**
	 * Get the name of the MilitaryTransport
	 * 
	 * @return the name of the MilitaryTransport
	 */
	protected String getName() {
		return name;
	}

	/**
	 * Get the defense of the MilitaryTransport
	 * 
	 * @return the defense of the MilitaryTransport
	 */
	protected int getDefense() {
		return defense;
	}

	/**
	 * Set the name of the MilitaryTransport
	 * 
	 * @param name
	 *            the name of the MilitaryTransport
	 */
	protected void setName(String name) {
		this.name = name;
	}

	/**
	 * Set the defense of the MilitaryTransport
	 * 
	 * @param defense
	 *            the defense of the MilitaryTransport
	 */
	protected void setDefense(int defense) {
		this.defense = defense;
	}

	/**
	 * Two MilitaryTransports interact together, one is the attacker and the
	 * other is the defender
	 * 
	 * @param t
	 *            The defender
	 */
	public abstract void interact(MilitaryTransport t);

	/**
	 * Make noise when interacting
	 */
	public abstract void makeNoise();

	/**
	 * Print out in format name,defense
	 */
	@Override
	public String toString() {
		return this.name + "," + this.defense;
	}

}
