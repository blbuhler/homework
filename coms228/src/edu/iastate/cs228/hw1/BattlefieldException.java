package edu.iastate.cs228.hw1;

/**
 * The Exception Class in the Battlefield
 * 
 * @author Siyu Lin
 * 
 */
public class BattlefieldException extends Exception {

	/**
	 * The constructor will take in an error message as a String and pass that
	 * string to the super constructor of Exception
	 * 
	 * @param message
	 *            A message to be printed out
	 */
	public BattlefieldException(String message) {
		super(message);
	}
}
