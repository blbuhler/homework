package edu.iastate.cs228.hw3;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * A GroupList class in which every cell has a CS228DoublyLinkedList
 * 
 * @author Siyu Lin
 * 
 * @param <E>
 */
public class GroupList<E> implements CS228List<E> {

	/**
	 * How large any particular CS228DoublyLinkedList<E> can get in each cell of
	 * listArray
	 */
	private int limit;
	/**
	 * Keep Track of how many elements exists within the entire GroupList
	 */
	private int size;
	/**
	 * The back store of the GroupList
	 */
	private CS228DoublyLinkedList<E>[] listArray;
	/**
	 * The default size of the listArray
	 */
	private static final int DEFAULT_CAPACITY = 3;

	/**
	 * Default constructor for the group list
	 */
	public GroupList() {
		this(3);
	}

	/**
	 * Construct a Group List by given limit
	 * 
	 * @param limit
	 *            The max items in every cell
	 */
	public GroupList(int limit) {
		if (limit <= 0) {
			throw new IllegalArgumentException();
		}
		this.limit = limit;
		emptyListArray();
		size = 0;
	}

	/**
	 * Empty the whole list array by setting it to the default capacity
	 */
	private void emptyListArray() {
		listArray = (CS228DoublyLinkedList<E>[]) new CS228DoublyLinkedList[DEFAULT_CAPACITY];
		for (int i = 0; i < listArray.length; i++) {
			listArray[i] = new CS228DoublyLinkedList<E>();
		}
	}

	/**
	 * Appends the specified element to the end of this list
	 * 
	 * @param toAdd
	 *            Element to add to the end of the list
	 */
	@Override
	public void add(E toAdd) {
		add(size, toAdd);
	}

	/**
	 * Inserts the specified element at the specified position in this list,
	 * shifts elements to the right if necessary
	 * 
	 * @param index
	 *            Add the element at this logical index of the list
	 * @param toAdd
	 *            Element to add to the list
	 * @return returns true if add is successful, false if not
	 * @throws IllegalArgumentException
	 *             throws IllegalArgumentException if index>size
	 */
	@Override
	public void add(int index, E toAdd) throws IllegalArgumentException {
		if (index < 0 || index > size) {
			throw new IllegalArgumentException("" + index);
		}
		if (toAdd == null) {
			throw new NullPointerException();
		}
		// Get the cell of the index
		int cell = index / limit;
		// Get the index in the cell
		int indexInCell = index % limit;
		checkCapacity();
		listArray[cell].add(indexInCell, toAdd);
		size++;
		adjustCell(cell);
	}

	/**
	 * Clears list of all elements
	 */
	@Override
	public void clear() {
		size = 0;
		emptyListArray();
	}

	/**
	 * Compares the specified object with this list for equality.
	 * 
	 * @param o
	 *            Object to compare to
	 * @return true if they are equal
	 */
	@Override
	public boolean equals(Object o) {
		if (o.getClass() != this.getClass() || o == null) {
			return false;
		} else {
			GroupList<E> other = (GroupList<E>) o;
			if (other.size() != size()) {
				return false;
			} else {
				for (int i = 0; i < size; i++) {
					if (!get(i).equals(other.get(i))) {
						return false;
					}
				}
				return true;
			}
		}
	}

	/**
	 * Tells us if the list is empty
	 * 
	 * @return true if there are no elements in the list
	 */
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns element at given index
	 * 
	 * @param index
	 *            index of list
	 * @return element at index
	 * @throws IllegalArgumentException
	 *             throw exception if index<0 or index>=size
	 */
	@Override
	public E get(int index) throws IllegalArgumentException {
		if (index < 0 || index >= size) {
			throw new IllegalArgumentException("" + index);
		}
		int cell = index / limit;
		int indexInCell = index % limit;
		return listArray[cell].get(indexInCell);
	}

	/**
	 * Returns the index of the first occurrence of the specified element in
	 * this list, or -1 if this list does not contain the element.
	 * 
	 * @param e
	 *            Object to compare to
	 * @return the index of the first occurrence of the specified element in
	 *         this list
	 */
	@Override
	public int indexOf(Object e) {
		if (e == null) {
			throw new NullPointerException();
		}
		GroupListIterator iter = new GroupListIterator();
		int i = 0;
		while (iter.hasNext()) {
			if (e.equals(iter.next())) {
				return i;
			}
			i++;
		}
		return -1;
	}

	/**
	 * Create a forward-only iterator
	 * 
	 * @return
	 */
	@Override
	public Iterator<E> iterator() {
		return new GroupListIterator();
	}

	/**
	 * Create a list-iterator at index 0
	 * 
	 * @return Returns a list iterator over the elements in this list (in proper
	 *         sequence).
	 */
	@Override
	public ListIterator<E> listIterator() {
		return new GroupListIterator();
	}

	/**
	 * Create a list-iterator at given index
	 * 
	 * @param index
	 *            index to create iterator
	 * @return
	 * @throws IllegalArgumentException
	 */
	@Override
	public ListIterator<E> listIterator(int index)
			throws IllegalArgumentException {
		if (index < 0 || index > size) {
			throw new IllegalArgumentException("" + index);
		}
		return new GroupListIterator(index);
	}

	/**
	 * Remove element at a given index
	 * 
	 * @param index
	 *            index of element to remove
	 * @return return the element that was removed
	 * @throws IllegalArgumentException
	 *             throw exception if index<0 or index>size
	 */
	@Override
	public E remove(int index) throws IllegalArgumentException {
		if (index < 0 || index >= size) {
			throw new IllegalArgumentException("" + index);
		}
		int cell = index / limit;
		int indexInCell = index % limit;
		ListIterator<E> cellIter = listArray[cell].listIterator(indexInCell);
		E elementToRemove = cellIter.next();
		cellIter.remove();
		size--;

		adjustCell(cell);
		checkCapacity();
		return elementToRemove;
	}

	/**
	 * Adjust the cell whenever adding an element or removing an element
	 * 
	 * @param cell
	 *            The cell to be adjusted
	 */
	private void adjustCell(int cell) {
		// If the size of the cell is less than limit and the next cell is not
		// empty, shift remaining elements up.
		if ((listArray[cell].size() < limit) && (getCells() > cell)
				&& !(listArray[cell + 1].isEmpty())) {
			listArray[cell].add(limit - 1, listArray[cell + 1].get(0));
			// When next cell is the last cell
			if ((cell + 1) == getCells()) {
				listArray[cell + 1].remove(0);
			}
			// Add the first element of the next cell to the current cell
			// Remove the first element of the array
			for (int i = cell + 1; i < getCells(); i++) {
				listArray[i].add(limit, listArray[i + 1].get(0));
				listArray[i].remove(0);
			}
		}

		// When the cell's size is greater than limit, move the last element to
		// the next cell
		if (listArray[cell].size() > limit) {
			// For the current cell and remaining cells
			// Add the last element to the the next cell
			for (int i = cell; i < getCells(); i++) {
				listArray[i + 1].add(0, listArray[i].get(limit));
			}
			// Remove the last element of the array
			for (int i = cell; i < getCells(); i++) {
				if (listArray[i].size() > limit) {
					listArray[i].remove(limit);
				}
			}
		}
	}

	/**
	 * Removes the first occurrence of the specified element from this list, if
	 * it is present
	 * 
	 * @param object
	 *            object to remove
	 * @return true if element was successfully removed
	 */
	@Override
	public boolean remove(E object) {
		if (indexOf(object) != -1) {
			remove(indexOf(object));
			return true;
		}
		return false;
	}

	/**
	 * Get the size (number of elements) in the list
	 * 
	 * @return return the size of the list
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element
	 * 
	 * @param index
	 *            index of element to replace
	 * @param element
	 *            element to place in list
	 * @return return element that was replaced
	 * @throws IllegalArgumentException
	 *             throw exception if index<0 or index>size
	 */
	@Override
	public E set(int index, E element) throws IllegalArgumentException {
		if (index < 0 || index >= size) {
			throw new IllegalArgumentException("" + index);
		}
		if (element == null) {
			throw new NullPointerException();
		}
		int cell = index / limit;
		int indexInCell = index % limit;
		E elementToSet = listArray[cell].get(indexInCell);
		listArray[cell].set(indexInCell, element);
		return elementToSet;
	}

	/**
	 * Return true if every cell in the listArray is full
	 * 
	 * @return true when the whole array is full
	 */
	private boolean isFull() {
		if (size() != 0) {

			for (CS228DoublyLinkedList<E> cell : listArray) {
				if (cellHasNull(cell)) {
					return false;
				}
			}

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return true if the cell has null elements
	 * 
	 * @param cell
	 * @return true if the cell has null elements
	 */
	private boolean cellHasNull(CS228DoublyLinkedList<E> cell) {
		if (cell.size() < limit) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check the last half of cells are null
	 * 
	 * @param list
	 *            An array of CS228DoublyLinkedList<E>
	 * @return true if last half cells are null
	 */
	private boolean halfNull(CS228DoublyLinkedList<E>[] list) {
		// We don't check when there are only three cells
		if (size() != 0 && list.length > 3) {
			int cells = getCells();
			int halfCell = cells / 2;
			// The first half cells should not contain null elements
			for (int i = 0; i < halfCell; i++) {
				if (cellHasNull(list[i])) {
					return false;
				}
			}
			// The next half cells should all be null elements
			for (int i = halfCell; i < cells; i++) {
				if (!cellHasNull(list[i])) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Return the rightmost cell in the listArray
	 * 
	 * @return the rightmost cell in the listArray
	 */
	private int getCells() {
		return size() / limit;
	}

	/**
	 * Adjust the capacity of the listArray. If half of the array is empty,
	 * reduce it to 3/4 of its size. If every cell is full, add 1/2 to its
	 * original size
	 */
	private void checkCapacity() {
		int cells = getCells();
		if (isFull()) {
			CS228DoublyLinkedList<E>[] tempArray = (CS228DoublyLinkedList<E>[]) new CS228DoublyLinkedList[cells * 3 / 2];
			for (int i = 0; i < tempArray.length; i++) {
				tempArray[i] = new CS228DoublyLinkedList<E>();
			}
			for (int i = 0; i < listArray.length; i++) {
				tempArray[i] = listArray[i];
			}
			listArray = tempArray;
		} else if (listArray.length > 3 && halfNull(listArray)) {
			CS228DoublyLinkedList<E>[] tempArray = (CS228DoublyLinkedList<E>[]) new CS228DoublyLinkedList[cells * 3 / 4];
			for (int i = 0; i < tempArray.length; i++) {
				tempArray[i] = new CS228DoublyLinkedList<E>();
			}
			for (int i = 0; i < tempArray.length; i++) {
				tempArray[i] = listArray[i];
			}
			listArray = tempArray;
		}
	}

	/**
	 * Implementation of ListIterator for this class
	 * 
	 * @author Siyu Lin
	 * 
	 */
	private class GroupListIterator implements ListIterator<E> {

		// Class invariants:
		// 1) logical cursor position is always between cursor.previous and
		// cursor
		// 2) after a call to next(), cursor.previous refers to the node just
		// returned
		// 3) after a call to previous() cursor refers to the node just returned
		// 4) index is always the logical index of node pointed to by cursor
		// 5) direction is BEHIND if last operation was next(),
		// AHEAD if last operation was previous(), NONE otherwise

		// direction for remove() and set()
		private static final int BEHIND = -1;
		private static final int AHEAD = 1;
		private static final int NONE = 0;

		int cursor;
		int currentCell;
		private int direction;

		public GroupListIterator() {
			this(0);
		}

		public GroupListIterator(int index) {
			if (index < 0 || index > size) {
				throw new IllegalArgumentException();
			}
			this.cursor = index;
			currentCell = index / limit;
			direction = NONE;
		}

		/**
		 * Returns true if this list iterator has more elements when traversing
		 * the list in the forward direction.
		 */
		@Override
		public boolean hasNext() {
			return cursor < size;
		}

		/**
		 * Returns the next element in the list and advances the cursor
		 * position.
		 */
		@Override
		public E next() {
			if (hasNext()) {

				ListIterator<E> cellIter = getCellIterator(cursor);

				E nextElement = cellIter.next();

				direction = BEHIND;
				cursor++;
				// When the cursor is at the head of the list
				if (cursorInHead()) {
					currentCell++;
				}

				return nextElement;

			} else {
				throw new NoSuchElementException();
			}
		}

		/**
		 * Returns true if this list iterator has more elements when traversing
		 * the list in the reverse direction.
		 */
		@Override
		public boolean hasPrevious() {
			return cursor > 0;
		}

		/**
		 * Returns the previous element in the list and moves the cursor
		 * position backwards.
		 */
		@Override
		public E previous() {
			if (hasPrevious()) {
				ListIterator<E> cellIter;
				// When the cursor is at the head of the list
				if (cursorInHead()) {
					currentCell--;
					cellIter = listArray[currentCell].listIterator(limit);
				}
				else{
					cellIter = getCellIterator(cursor);
				}
				
				E prevElement = cellIter.previous();

				cursor--;
				direction = AHEAD;

				return prevElement;
			} else {
				throw new NoSuchElementException();
			}
		}

		/**
		 * Return true if the position of the cursor is in the head of the cell
		 * 
		 * @return true when the cursor is in head of the CSDoublyLinkedList
		 */
		private boolean cursorInHead() {
			if (cursor % limit == 0) {
				return true;
			}
			return false;
		}

		/**
		 * Get the iterator of a specific cell according to the given cursor
		 * 
		 * @param cursor
		 *            the position of the cursor
		 * @return the iterator of a specific cell
		 */
		private ListIterator<E> getCellIterator(int cursor) {
			// Get the current cell
			CS228DoublyLinkedList<E> cell = listArray[cursor / limit];
			// Get the cursor's position in the cell
			int cursorInCell = cursor % limit;
			ListIterator<E> cellIter = cell.listIterator(cursorInCell);
			return cellIter;
		}

		/**
		 * Returns the index of the element that would be returned by a
		 * subsequent call to next().
		 */
		@Override
		public int nextIndex() {
			return cursor;
		}

		/**
		 * Returns the index of the element that would be returned by a
		 * subsequent call to previous().
		 */
		@Override
		public int previousIndex() {
			return cursor - 1;
		}

		/**
		 * Removes from the list the last element that was returned by next() or
		 * previous() (optional operation).
		 */
		@Override
		public void remove() {

			if (direction == NONE) {
				throw new IllegalStateException();
			}

			else {
				if (direction == AHEAD) {

					// remove node at cursor and move to next node
					int currentPosition = cursor;
					while (cursor < size - 1) {
						set(get(cursor + 1));
						cursor++;
					}

					removeLastElement();

					cursor = currentPosition;

				} else {
					// remove node behind cursor and adjust index
					int currentPosition = cursor;
					while (cursor < size) {
						set(get(cursor));
						cursor++;
					}

					removeLastElement();

					// When the cursor is at the head of the cell
					if (cursorInHead()) {
						currentCell--;
					}
					cursor = currentPosition - 1;
				}
			}
			--size;
			checkCapacity();
			direction = NONE;
		}

		/**
		 * Remove the last element of the GroupList
		 */
		private void removeLastElement() {
			// Remove the last element
			int lastCell = (size - 1) / limit;
			int lastElement = (size - 1) % limit;
			listArray[lastCell].remove(lastElement);
		}

		/**
		 * Replaces the last element returned by next() or previous() with the
		 * specified element (optional operation).
		 */
		@Override
		public void set(E e) {
			if (direction == NONE) {
				throw new IllegalStateException();
			}
			if (e == null) {
				throw new NullPointerException();
			}

			ListIterator<E> cellIter;
			if (direction == AHEAD) {
				cellIter = getCellIterator(cursor);
				// Set the element under the cursor
				cellIter.next();
			} else {
				// Set the element before the cursor
				// Precondition: After calling next()
				if (!hasPrevious()) {
					throw new NoSuchElementException();
				}
				if (cursorInHead()) {
					cellIter = listArray[currentCell - 1].listIterator(limit);
				} else {
					cellIter = getCellIterator(cursor);
				}
				cellIter.previous();
			}
			cellIter.set(e);
		}

		/**
		 * Inserts the specified element into the list
		 */
		@Override
		public void add(E e) {
			if (e == null) {
				throw new NullPointerException();
			}
			int currentPos = cursor;
			GroupList.this.add(cursor, e);
			// Move the cursor to next element
			cursor = currentPos + 1;
			// When the cursor is at the head of the list
			if (cursorInHead()) {
				currentCell++;
			}
			direction = NONE;
		}
	}

	public CS228DoublyLinkedList<E>[] getListArray() {
		return listArray;
	}

}
