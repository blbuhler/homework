package edu.iastate.cs228.hw3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.ListIterator;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class GroupListTest {
	GroupList<String> defaultGroupList;
	ListIterator<String> iterator;

	@Before
	public void setup() {
		defaultGroupList = new GroupList<String>();
	}

	@Test
	public void testInitialization() {
		assertEquals(defaultGroupList.size(), 0);
	}

	@Test
	public void testAdd() {
		fillNineElements(defaultGroupList);
		assertEquals(defaultGroupList.size(), 9);
		defaultGroupList.add(4, "Q");
		assertEquals(defaultGroupList.size(), 10);
		assertEquals(defaultGroupList.get(3), "B");
		assertEquals(defaultGroupList.get(4), "Q");
		assertEquals(defaultGroupList.get(5), "S");
		assertEquals(defaultGroupList.get(6), "R");
		assertEquals(defaultGroupList.get(9), "T");
	}

	@Test
	public void testAddMoreThanCapacity() {
		fillNineElements(defaultGroupList);
		fillNineElements(defaultGroupList);
		fillNineElements(defaultGroupList);
		assertEquals(defaultGroupList.size(), 27);
		assertEquals(defaultGroupList.get(26), "T");
	}

	// @Ignore
	@Test
	public void testRemovePostCondition() {
		fillNineElements(defaultGroupList);
		defaultGroupList.remove(4);
		ListIterator<String> iterator = defaultGroupList.listIterator(4);
		assertEquals(defaultGroupList.get(4), "R");
		assertEquals(iterator.next(), "R");
	}

	// @Ignore
	@Test
	public void testGroupListIteratorInTailOfTheCell() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		// Add 1 more
		defaultGroupList.add("A");
		iterator = defaultGroupList.listIterator(8);
		assertEquals(iterator.nextIndex(), 8);
		assertEquals(iterator.next(), "T");
		assertEquals(iterator.next(), "A");
	}

	// @Ignore
	@Test
	public void testGroupListIteratorInHeadOfTheCell() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		// Add 1 more
		defaultGroupList.add("A");
		iterator = defaultGroupList.listIterator(9);
		assertEquals(iterator.previousIndex(), 8);
		assertEquals(iterator.previous(), "T");
		assertEquals(iterator.next(), "T");
		assertEquals(iterator.next(), "A");
		assertEquals(defaultGroupList.size(), 10);
	}

	// @Ignore
	@Test
	public void testGroupListIteratorRemove() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		// Add 1 more
		defaultGroupList.add("A");
		iterator = defaultGroupList.listIterator(8);
		assertEquals(iterator.next(), "T");
		assertEquals(defaultGroupList.get(8), "T");
		// Remove T
		iterator.remove();
		assertEquals(defaultGroupList.get(8), "A");
		// Add 1 more
		defaultGroupList.add("B");
		assertEquals(defaultGroupList.get(9), "B");
		assertEquals(defaultGroupList.size(), 10);
	}

	@Test
	public void testGroupListIteratorNextSet() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		iterator = defaultGroupList.listIterator();
		assertEquals(iterator.next(), "A");
		iterator.set("B");
		assertEquals(defaultGroupList.get(0), "B");
		assertEquals(defaultGroupList.get(1), "T");
		assertEquals(defaultGroupList.size(), 9);
	}
	
	@Test
	public void testGroupListIteratorInTailNextSet() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		iterator = defaultGroupList.listIterator(8);
		assertEquals(iterator.next(), "T");
		iterator.set("B");
		assertEquals(defaultGroupList.get(8), "B");
		assertEquals(defaultGroupList.get(7), "A");
		assertEquals(defaultGroupList.size(), 9);
	}
	
	@Test
	public void testGroupListIteratorInHeadPreviousSet() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		iterator = defaultGroupList.listIterator(6);
		assertEquals(iterator.previous(), "R");
		iterator.set("B");
		assertEquals(defaultGroupList.get(5), "B");
		assertEquals(defaultGroupList.get(6), "E");
		assertEquals(defaultGroupList.size(), 9);
	}

	@Test
	public void testGroupListIteratorPreviousSet() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		iterator = defaultGroupList.listIterator(1);
		assertEquals(iterator.previous(), "A");
		iterator.set("B");
		assertEquals(defaultGroupList.get(0), "B");
		assertEquals(defaultGroupList.get(1), "T");
		assertEquals(defaultGroupList.size(), 9);
	}

	@Test
	public void testClearThenAdd() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		// Clear
		defaultGroupList.clear();
		assertTrue(defaultGroupList.isEmpty());
		defaultGroupList.add("A");
		assertEquals(defaultGroupList.get(0), "A");
	}

	@Test
	public void testIteratorNextRemoveElementInTail() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		iterator = defaultGroupList.listIterator(5);
		assertEquals(iterator.next(), "R");
		// Remove "R"
		iterator.remove();
		assertEquals(iterator.next(), "E");
		assertEquals(iterator.next(), "A");
		assertEquals(iterator.previous(), "A");
		assertEquals(defaultGroupList.size(), 8);
	}

	@Test
	public void testIteratorPreviousRemoveElementInTail() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		iterator = defaultGroupList.listIterator(6);
		assertEquals(iterator.previous(), "R");
		// Remove "R"
		iterator.remove();
		assertEquals(iterator.next(), "E");
		assertEquals(iterator.next(), "A");
		assertEquals(iterator.next(), "T");
		assertEquals(iterator.previous(), "T");
		assertEquals(defaultGroupList.size(), 8);
	}

	@Test
	public void findIndex() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		assertEquals(defaultGroupList.indexOf("A"), 0);
		assertEquals(defaultGroupList.indexOf("C"), -1);
	}
	
	@Test (expected = NullPointerException.class)
	public void findIndexOfNull() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		defaultGroupList.indexOf(null);
	}

	@Test
	public void testIteratorNextAdd() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		iterator = defaultGroupList.listIterator(2);
		assertEquals(iterator.next(), "Z");
		// Add under the iterator
		iterator.add("C");
		assertEquals(iterator.next(), "B");
		assertEquals(iterator.next(), "S");
		assertEquals(defaultGroupList.indexOf("C"), 3);
		assertEquals(defaultGroupList.indexOf("Z"), 2);
		assertEquals(defaultGroupList.size(), 10);
	}

	@Test
	public void testIteratorPreviousAdd() {
		// Add 9 elements
		fillNineElements(defaultGroupList);
		iterator = defaultGroupList.listIterator(3);
		assertEquals(iterator.previous(), "Z");
		// Add under the iterator
		iterator.add("C");
		assertEquals(iterator.next(), "Z");
		assertEquals(defaultGroupList.size(), 10);
	}

	private void removeHalfCells(GroupList list) {
		int cells;
		if (list.size() != 0) {
			if (list.size() % 3 == 0) {
				cells = list.size() / 3;
			} else {
				cells = list.size() / 3 + 1;
			}
			int firstElementInMidCell = (cells / 2) * 3;
			// Remove until it reaches the first element of the mid of the cells
			for (int i = list.size(); i < firstElementInMidCell; i--) {
				list.remove(list.size() - 1);
			}
		}
	}

	@Test
	public void testEquals() {
		GroupList<String> testGroupList = new GroupList<String>();
		fillNineElements(testGroupList);
		fillNineElements(defaultGroupList);
		assertTrue(testGroupList.equals(defaultGroupList));
	}

	@Test
	public void testEquals2() {
		GroupList<String> testGroupList = new GroupList<String>();
		fillNineElements(testGroupList);
		testGroupList.add("X");
		assertFalse(testGroupList.equals(defaultGroupList));
	}

	@Test
	public void testEquals3() {
		GroupList<String> testGroupList = new GroupList<String>();
		testGroupList.add("X");
		assertFalse(testGroupList.equals(defaultGroupList));
	}

	@Test
	public void testEquals4() {
		GroupList<Integer> testGroupList = new GroupList<Integer>();
		assertTrue(testGroupList.equals(defaultGroupList));
	}
	
	@Test (expected = NullPointerException.class)
	public void testEquals5() {
		GroupList<Integer> testGroupList = new GroupList<Integer>();
		testGroupList.add(1);
		assertTrue(testGroupList.equals(null));
	}

	@Test(expected = NullPointerException.class)
	public void findNull() {
		defaultGroupList.indexOf(null);
	}

	@Test(expected = NullPointerException.class)
	public void addNull() {
		defaultGroupList.add(null);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testIllegalIndex(){
		fillNineElements(defaultGroupList);
		defaultGroupList.add(-1, "A");
		defaultGroupList.add(10, "A");
		defaultGroupList.remove(10);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testIllegalIndexIterator(){
		fillNineElements(defaultGroupList);
		iterator  = defaultGroupList.listIterator(-1);
		iterator  = defaultGroupList.listIterator(10);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testIllegalConstructor(){
		GroupList<String> testGroupList = new GroupList<String>(0);
	}
	
	private void fillNineElements(GroupList<String> list) {
		list.add("A");
		list.add("T");
		list.add("Z");
		list.add("B");
		list.add("S");
		list.add("R");
		list.add("E");
		list.add("A");
		list.add("T");
	}
}
