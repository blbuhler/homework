package edu.iastate.cs228.hw2;

/**
 * A generic class Pair whose two elements can take two different types and both
 * types all implement Comparable interface
 * 
 * @author Siyu Lin
 * 
 * @param <E>
 *            One type which implements Comparable<E>
 * @param <T>
 *            Another type which implements Comparable<T>
 */
public class Pair<E extends Comparable<E>, T extends Comparable<T>> implements
		Comparable<Pair<E, T>> {

	/**
	 * The first element of pair, whose type is E
	 */
	private E first;
	/**
	 * The second element of pair, whose type is T
	 */
	private T second;

	/**
	 * Constructor for Pair takes two parameters of type E and type T
	 * respectively
	 * 
	 * @param first
	 *            The first element of Pair, whose type is E
	 * @param second
	 *            The second element of Pair, whose type is T
	 */
	public Pair(E first, T second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * Pair can be compared to each other, according to their first elements and
	 * then according to their second elements
	 */
	@Override
	public int compareTo(Pair<E, T> other) {
		if (this.getFirst().compareTo(other.getFirst()) > 0) {
			return 1;
		} else if (this.getFirst().compareTo(other.getFirst()) < 0) {
			return -1;
		} else {
			
			//If their first elements are equal, we compare their second elements
			if (this.getSecond().compareTo(other.getSecond()) > 0) {
				return 1;
			} else if (this.getSecond().compareTo(other.getSecond()) < 0) {
				return -1;

			} else {
				return 0;
			}
		}
	}

	/**
	 * Get the first element
	 * @return the first element of type E
	 */
	public E getFirst() {
		return first;
	}

	/**
	 * Get the second element
	 * @return the second element of type T
	 */
	public T getSecond() {
		return second;
	}

}
