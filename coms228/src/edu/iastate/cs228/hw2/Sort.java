package edu.iastate.cs228.hw2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

/**
 * Sort class consists of selection sort, insertion sort and quick sort. And all
 * of them are generic methods.
 * 
 * @author Siyu Lin
 * 
 */
public class Sort {
	/**
	 * The counter for quickSort
	 */
	private static long quickSortCounter;

	/**
	 * Sort an array of type T using selection sort
	 * 
	 * @param arr
	 *            Array of type T to be sorted
	 * @param cp
	 *            Comparator for the array
	 * @return the comparisons performed during the execution of the sorting
	 *         algorithm
	 */
	public static <T> long selectionSort(T[] arr, Comparator<? super T> cp) {
		if (!arrayValid(arr) || !comparatorValid(cp)) {
			return 0;
		}

		long count = 0;

		for (int i = 0; i < arr.length; i++) {
			int minIndex = i;
			// Find the Index of min
			for (int j = i + 1; j < arr.length; j++) {
				// The counter only increment when making comparisons
				count++;
				if (cp.compare(arr[j], arr[minIndex]) < 0) {

					minIndex = j;
				}
			}
			// The counter does not increment when do swapping
			// count++;
			swap(arr, i, minIndex);
		}

		return count;
	}

	/**
	 * Sort an array of type T using insertion sort
	 * 
	 * @param arr
	 *            Array of type T to be sorted
	 * @param cp
	 *            Comparator for the array
	 * @return the comparisons performed during the execution of the sorting
	 *         algorithm
	 */
	public static <T> long insertionSort(T[] arr, Comparator<? super T> cp) {
		if (!arrayValid(arr) || !comparatorValid(cp)) {
			return 0;
		}

		long count = 0;
		T[] tempArray = (T[]) new Object[arr.length];
		// Choose the first element and Copy it to a dummy array
		tempArray[0] = arr[0];

		// Insert to a sorted array
		for (int i = 1; i < arr.length; i++) {
			T current = arr[i];
			int j = i;
			while (j > 0 && cp.compare(arr[j - 1], current) > 0) {
				arr[j] = arr[j - 1];
				j--;
				// The counter only increments when making comparisons
				count++;
			}
			// The counter does not increment in the loop
			// count++;
			arr[j] = current;
		}
		return count;
	}

	/**
	 * Sort an array of type T using quick sort
	 * 
	 * @param arr
	 *            Array of type T to be sorted
	 * @param cp
	 *            Comparator for the array
	 * @return the comparisons performed during the execution of the sorting
	 *         algorithm
	 */
	public static <T> long quickSort(T[] arr, Comparator<? super T> cp,
			String ptype) {
		quickSortCounter = 0;
		recursiveQuicksort(arr, cp, 0, arr.length - 1, ptype);
		return quickSortCounter;
	}

	/**
	 * Check if the array is null or any element is null
	 * 
	 * @param arr
	 *            The array to be sorted
	 * @return true if the array is null or any element is null
	 */
	private static <T> boolean arrayValid(T[] arr) {
		if (arr == null) {
			return false;
		} else {

			for (T e : arr) {
				if (e == null) {
					return false;
				}
			}

			return true;
		}
	}

	/**
	 * Check the comparator is valid
	 * 
	 * @param cp
	 *            The comparator to be used in the sorting algorithm
	 * @return false if the comparator is null, true otherwise
	 */
	private static <T> boolean comparatorValid(Comparator<? super T> cp) {
		if (cp == null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Partition part in the quick sort algorithm. Postcondition: The elements
	 * before the pivot are all smaller than the pivot and the elements after
	 * the pivot are all greater than the pivot.
	 * 
	 * @param arr
	 *            The array to be sorted in the quick sort
	 * @param cp
	 *            The comparator used in the quick sort
	 * @param low
	 *            The lower bound of the part to be sorted
	 * @param high
	 *            The higher bound of the part to be sorted
	 * @param ptype
	 *            The type of pivot, which can be the first element, or the
	 *            median of the first, middle, last element, or a random element
	 * @return an array partitioned into two parts. The left part consists of
	 *         elements smaller than the pivot and the right part consists of
	 *         elements greater than the pivot
	 */
	private static <T> int partition(T[] arr, Comparator<? super T> cp,
			int low, int high, String ptype) {
		int pivotPosition = getPivot(arr, cp, low, high, ptype);

		if (low < high) {
			// Counter does not increment when swapping values
			// quickSortCounter++;
			int rs = low;
			int ls = high;
			T pivotValue = arr[pivotPosition];
			// Move the pivot to the end of the array
			swap(arr, pivotPosition, ls);
			pivotPosition = ls;
			while (rs < ls) {
				// Counter does not increment in the loop
				// quickSortCounter++;
				if (cp.compare(arr[rs], pivotValue) > 0) {
					// The first comparison
					quickSortCounter++;
					if (cp.compare(arr[ls], pivotValue) < 0) {
						// The second comparison
						swap(arr, rs, ls);
						quickSortCounter++;
						rs++;
					} else {
						// The second comparison
						quickSortCounter++;
						ls--;
					}
				} else {
					// The first comparison
					quickSortCounter++;
					rs++;
				}
			}

			// Swap the value of pivot with left scanner's position
			// Counter does not increment when swapping values
			swap(arr, pivotPosition, ls);
			pivotPosition = ls;

			// quickSortCounter++;
		}
		return pivotPosition;
	}

	/**
	 * Get the pivot's position according to the user's ptype in the quick sort
	 * 
	 * @param arr
	 *            The array to be sorted
	 * @param cp
	 *            The comparator used in the quick sort
	 * @param low
	 *            The lower bound of the part to be sorted, inclusive
	 * @param high
	 *            The higher bound of the part to be sorted, inclusive
	 * @param ptype
	 *            The type of pivot, which can be the first element, or the
	 *            median of the first, middle, last element, or a random element
	 * @return the pivot's position in the array
	 */
	private static <T> int getPivot(T[] arr, Comparator<? super T> cp, int low,
			int high, String ptype) {
		int pivotPosition = low;
		if (ptype.equals("first")) {
			pivotPosition = low;
		} else if (ptype.equals("median")) {
			int mid = (low + high) / 2;

			// Select the median of three values
			if (cp.compare(arr[low], arr[mid]) > 0) {
				if (cp.compare(arr[mid], arr[high]) > 0) {
					pivotPosition = mid;
				} else if (cp.compare(arr[low], arr[high]) > 0) {
					pivotPosition = high;
				} else {
					pivotPosition = low;
				}
			} else {
				if (cp.compare(arr[low], arr[high]) > 0) {
					pivotPosition = low;
				} else if (cp.compare(arr[mid], arr[high]) > 0) {
					pivotPosition = high;
				} else {
					pivotPosition = mid;
				}
			}

		} else if (ptype.equals("random")) {
			Random rd = new Random();
			// Generate a number from 0 to high-low
			int range = rd.nextInt(high - low + 1);
			pivotPosition = low + range;
		} else {
			throw new IllegalArgumentException("The ptype is not valid");
		}
		return pivotPosition;
	}

	/**
	 * Initializing the recursion in quick sort
	 * 
	 * @param arr
	 *            The array to be sorted
	 * @param cp
	 *            The comparator used in the quick sort
	 * @param low
	 *            The lower bound of the part to be sorted, inclusive
	 * @param high
	 *            The higher bound of the part to be sorted, inclusive
	 * @param ptype
	 *            The type of pivot, which can be the first element, or the
	 *            median of the first, middle, last element, or a random element
	 */
	private static <T> void recursiveQuicksort(T[] arr,
			Comparator<? super T> cp, int low, int high, String ptype) {
		int pivotPosition;
		//When low >= high, we assume that it is the base case
		if (low < high) {
			pivotPosition = partition(arr, cp, low, high, ptype);
			recursiveQuicksort(arr, cp, low, pivotPosition - 1, ptype);
			recursiveQuicksort(arr, cp, pivotPosition + 1, high, ptype);
		}
		
	}

	/**
	 * Swap two elements of an array of type T
	 * 
	 * @param arr
	 *            An array
	 * @param i
	 *            The position of element to be swapped
	 * @param j
	 *            The position of element to be swapped
	 */
	private static <T> void swap(T[] arr, int i, int j) {
		T temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}
