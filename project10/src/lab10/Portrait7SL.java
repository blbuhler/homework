package lab10;

import java.awt.Graphics;

public class Portrait7SL extends Portrait {
	public Portrait7SL() {
		super(0.4, 0.0, 0.3, 0.3, 0.1, 0.2);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		int midX = getWidth() / 2;

		// Draw a big eye right in the middle of the face.
		int eyeRadius1 = (int) (0.15 * SIZE);
		int eyeRadius2 = (int) (0.15 * SIZE);
//		g.fillOval(midX - eyeRadius, headRadius - eyeRadius * 2, 2 * eyeRadius,
//				2 * eyeRadius);

		// And give him a creepy smile.
		int smileRadius = (int) (0.5 * headRadius);
		g.drawArc(midX - smileRadius, (int) (0.8 * headRadius),
				smileRadius * 2, smileRadius * 2, 0, -180);
		
		//Eyes with Money
		g.drawString("$", midX - eyeRadius1, (int) (0.6 * headRadius));
		g.drawString("$", midX + eyeRadius2, (int) (0.6 * headRadius));
		
		//Nose
		int noseRadius = (int) (0.05 * SIZE);
		g.fillRect(midX, (int) (0.9 * headRadius), 4,  8);
		g.fillRect(midX, (int) (0.9 * headRadius), 3,  6);
	}
}
